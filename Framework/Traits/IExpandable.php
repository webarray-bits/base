<?php

namespace Framework\Traits;

interface IExpandable
{
    /**
     * Register an extension.
     *
     * @param string   $name
     * @param callable $extension
     *
     * @throws \Exception
     */
    public function registerExtension(string $name, $extension);

    /**
     * Unregister an extension.
     *
     * @param string $name
     */
    public function unregisterExtension(string $name);
}