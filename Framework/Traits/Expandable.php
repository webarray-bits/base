<?php

namespace Framework\Traits;

trait Expandable
{
    /**
     * All registered extensions.
     *
     * @var array
     */
    protected $extensions = [];

    /**
     * Execute a existing or registered function.
     *
     * @param string $name
     * @param array  $arguments
     *
     * @return mixed
     */
    public function __call(string $name, array $arguments)
    {
        // Check if an actual method is already defined:
        if (method_exists($this, $name)) {
            return call_user_func_array([$this, $name], $arguments);
        }

        // Check if the function has been registered.
        if (array_key_exists($name, $this->extensions)) {
            return $this->extensions[$name](...$arguments);
        }

        return null;
    }
}