<?php

namespace Framework\Event\Kernel;

use Symfony\Component\EventDispatcher\Event;

class KernelStartEvent extends Event
{
}
