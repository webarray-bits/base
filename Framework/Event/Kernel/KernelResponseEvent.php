<?php

namespace Framework\Event\Kernel;

use Framework\Http\Response;
use Symfony\Component\EventDispatcher\Event;

class KernelResponseEvent extends Event
{
    /**
     * @var Response
     */
    public $response;

    /**
     * KernelResponseEvent constructor.
     *
     * @param Response $response
     */
    public function __construct(Response $response)
    {
        $this->response = $response;
    }
}
