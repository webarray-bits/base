<?php

namespace Framework\Event;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcher;

class EventCarrier extends EventDispatcher
{
    /**
     * The listeners defined for the different events.
     * This is already available on the EventDispatcher, but is a private member.
     *
     * @var array
     */
    private $listeners = [];

    /**
     * The sorted event listeners.
     * This is already available on the EventDispatcher, but is a private member.
     *
     * @var array
     */
    private $sorted = [];

    /**
     * This class exists for this function.
     * The difference from the original implementation is that a '*' (asterisk) is accepted
     * as a listener. It means the listener will listen for all events that fire.
     *
     * This can be useful when logging, debugging or testing.
     *
     * @param null $eventName
     *
     * @return array|mixed
     */
    public function getListeners($eventName = null)
    {
        // When no event name was given.
        if ($eventName === null) {
            foreach ($this->listeners as $eventName => $listener) {
                if (!isset($this->sorted[$eventName])) {
                    krsort($this->listeners[$eventName]);
                    $this->sorted[$eventName] = array_merge(
                        $this->sorted[$eventName],
                        $this->listeners[$eventName]
                    );
                }
            }

            return array_filter($this->sorted);
        }

        $any_exists = array_key_exists('*', $this->listeners) && isset($this->listeners['*']);
        $event_exists = array_key_exists($eventName, $this->listeners) && isset($this->listeners[$eventName]);

        // Sort any if needed.
        if ($any_exists && !isset($this->sorted['*'])) {
            $this->sortListeners('*');
        }

        // Sort event if needed
        if (!isset($this->sorted[$eventName])) {
            $this->sortListeners($eventName);
        }

        if ($any_exists && !$event_exists) {
            return $this->sorted['*'];
        }

        if ($any_exists && $event_exists) {
            return array_merge($this->sorted['*'], $this->sorted[$eventName]);
        }

        if (!$any_exists && $event_exists) {
            return $this->sorted[$eventName];
        }

        if (!$any_exists && !$event_exists) {
            return [];
        }
    }

    /**
     * @param string     $eventName
     * @param Event|null $event
     *
     * @return Event
     */
    public function dispatch($eventName, Event $event = null)
    {
        if ($event === null) {
            $event = new Event();
        }

        $event->setDispatcher($this);
        $event->setName($eventName);

        $this->doDispatch($this->getListeners($eventName), $eventName, $event);

        return $event;
    }

    /**
     * Sorts the internal list of listeners for the given event by priority.
     *
     * @param string $eventName The name of the event
     */
    private function sortListeners($eventName)
    {
        krsort($this->listeners[$eventName]);
        $this->sorted[$eventName] = call_user_func_array('array_merge', $this->listeners[$eventName]);
    }
}
