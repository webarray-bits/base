<?php

namespace Framework\Event\Http;

use Framework\Http\Route;
use Symfony\Component\EventDispatcher\Event;

class HttpDispatchEvent extends Event
{
    /**
     * The route that has been dispatched.
     *
     * @var Route
     */
    public $route;

    /**
     * HttpDispatchEvent constructor.
     *
     * @param Route $route
     */
    public function __construct(Route $route)
    {
        $this->route = $route;
    }
}
