<?php

namespace Framework\Middleware;

/**
 * The Proxy class makes it possible to construct any arbitrary Middleware from string and arguments,
 * which combined with the middleware() helper in /Framework/Util/Helpers.php creates a nice interface for
 * defining Middleware in controllers. The interface is as follows:
 * $this->middleware =
 * [
 *      middleware(Authorize::class, 'Admin'),
 *      middleware(Form::class, $validator)
 * ].
 *
 * Class Proxy
 */
class Proxy
{
    /**
     * @var string
     */
    private $classname;

    /*
     * @var array
     */
    private $arguments;

    /**
     * Static array that matches the short name to the class name.
     *
     * @var array
     */
    public static $definitions = [];

    /**
     * Proxy constructor.
     *
     * @param string $class
     * @param $arguments
     */
    public function __construct(string $class, $arguments)
    {
        $this->classname = $class;
        $this->arguments = $arguments;
    }

    /**
     * Run the middleware.
     *
     * @return mixed
     */
    public function run()
    {
        if (!class_exists($this->classname)) { // A short name is set
            $this->classname = static::$definitions[$this->classname];
        }

        return call_user_func([$this->classname, 'run'], ...$this->arguments);
    }

    /**
     * @return string
     */
    public function getClassName():string
    {
        return $this->classname;
    }

    /**
     * @return mixed
     */
    public function getArguments()
    {
        return $this->arguments;
    }

    public function getFormattedArguments()
    {
        $ret = '';

        foreach ($this->arguments as $arg) {
            $ret .= $arg.', ';
        }

        return rtrim($ret, ', ');
    }
}
