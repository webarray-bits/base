<?php

namespace Framework\Middleware;

interface Middleware
{
    const Abort = 0;
    const Success = 1;

    public static function run():int;
}
