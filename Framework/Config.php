<?php

namespace Framework;

use Dotenv\Dotenv;

class Config
{
    public static function load(string $rootPath, string $configPath)
    {
        $path = $rootPath.$configPath;
        $config = [];

        if (!file_exists($path)) {
            throw new \Exception('The configuration directory '.$path.' doesn\'t exist.');
        }

        // Check for the enviroment variables
        if (file_exists($rootPath . '/.env')) {
            $env = new Dotenv($rootPath);
            $env->load();
        }

        // Index the configured config directory.
        $files = scandir($path);

        foreach ($files as $file) {
                // Only parse .php files.
            if (!preg_match('/.php$/', $file)) {
                continue;
            }

            // 'app.php' becomes 'App' as the key.
            $config[ucfirst(substr($file, 0, -4))] = include $path.'/'.$file;
        }

        // Replace the @ROOT with the correct directory
        foreach ($config as $key => $value) {
            $config[$key] = self::recursiveReplace($value, $rootPath);
        }

        return $config;
    }

    private static function recursiveReplace($value, $rootPath)
    {
        if (is_string($value)) {
            return str_replace('@ROOT', $rootPath, $value);
        } elseif (is_array($value)) {
            foreach ($value as $key => $val) {
                $value[$key] = self::recursiveReplace($val, $rootPath);
            }

            return $value;
        }

        return $value;
    }
}
