<?php

namespace Framework\Console;

use Framework\Container;
use Framework\Http\RouteRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RouteCommand extends Command
{
    /**
     * Configure the command name and arguments.
     */
    protected function configure()
    {
        $this->setName('route')
            ->setDescription('Route commands')
            ->addArgument(
                'print',
                InputArgument::OPTIONAL,
                'Print the currently defined routing table.'
            )
            ->addOption(
                'method',
                null,
                InputOption::VALUE_NONE,
                'Sort by method.'
            )
            ->addOption(
                'action',
                null,
                InputOption::VALUE_NONE,
                'Sort by action.'
            )
            ->addOption(
                'url',
                null,
                InputOption::VALUE_NONE,
                'Sort by url.'
            )
            ->addOption(
                'middleware',
                null,
                InputOption::VALUE_NONE,
                'Sort by middleware.'
            )
            ->addOption(
                'no-callable',
                null,
                InputOption::VALUE_NONE,
                'Exclude routes with a callable action.'
            );
    }

    /**
     * Run the command.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($input->getArgument('print')) {
            $routeObjects = Container::get(RouteRegistry::class)->getRoutes();

            $routes = [];
            foreach ($routeObjects as $o) {
                $routes[] = [$o->url, $o->method, is_callable($o->action) ? 'Callable' : $o->action, $o->getMiddlewareAsString()];
            }

            if ($input->getOption('url')) {
                usort($routes, function ($a, $b) {
                    return strcmp($a[0], $b[0]);
                });
            } elseif ($input->getOption('method')) {
                usort($routes, function ($a, $b) {
                    return strcmp($a[1], $b[1]);
                });
            } elseif ($input->getOption('action')) {
                usort($routes, function ($a, $b) {
                    return strcmp($a[2], $b[2]);
                });
            } elseif ($input->getOption('middleware')) {
                usort($routes, function ($a, $b) {
                    return strcmp($a[2], $b[2]);
                });
            }

            if ($input->getOption('no-callable')) {
                $routes = array_filter($routes, function ($route) {
                    return $route[2] !== 'Callable';
                });
            }

            $table = new Table($output);
            $table->setHeaders(['Url', 'Method', 'Action', 'Middleware']);

            foreach ($routes as $route) {
                $table->addRow($route);
            }

            $table->render();
        }
    }
}
