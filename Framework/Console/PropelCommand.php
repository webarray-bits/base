<?php

namespace Framework\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use function Stringy\create as s;

class PropelCommand extends Command
{
    /**
     * Configure the command name and arguments.
     */
    protected function configure()
    {
        $this->setName('db')
            ->setDescription('Run a Propel command.')
            ->addArgument('commands', InputArgument::REQUIRED, 'The command for Propel to execute');
    }

    /**
     * Run the command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // This will place you in the database folder so all commands will work properly
        chdir(root().'/Database');

        // Get OS-specific path
        $path = $this->getPath();

        switch (trim($input->getArgument('commands'))) {
            case 'renew':
                shell_exec($path.' sql:build');
                shell_exec($path.' sql:insert');
                shell_exec($path.' model:build');
                break;
            default:
                shell_exec($path.' '.$input->getArgument('commands'));
        }
    }

    /**
     * Get the correct path to the Propel scripts, based on operating system.
     * @return string
     */
    private function getPath(): string
    {
        if (s(PHP_OS)->toUpperCase()->startsWith('WIN')) {
            return escapeshellarg(root().'\vendor\bin\propel.bat');
        }

        return escapeshellarg(root().'/vendor/bin/propel');
    }
}
