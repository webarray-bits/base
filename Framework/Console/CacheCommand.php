<?php

namespace Framework\Console;

use Framework\Facades\Cache;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use function Stringy\create as s;

class CacheCommand extends Command
{
    /**
     * Configure the command name and arguments.
     */
    protected function configure()
    {
        $this->setName('cache')
            ->setDescription('Cache related commands.')
            ->addArgument('commands', InputArgument::REQUIRED, 'Clean the cache.');
    }

    /**
     * Run the command.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        switch (trim($input->getArgument('commands'))) {
            case 'clean':
                $this->cleanCache($output);
                break;
            case 'stats':
                dump(Cache::stats());
                break;
            default:
        }
    }

    /**
     * Clean the cache and cache files.
     *
     * @param OutputInterface $output
     */
    private function cleanCache(OutputInterface $output)
    {
        // Clean PHPFastCache
        Cache::clean();

        $output->writeln(ConsoleHelpers::info('PHPFastCache clean. Removing template cache...'));

        // Remove template cache files
        $this->emptyDirectory(__DIR__.'/../../Storage/Cache/Template');
        $output->writeln(ConsoleHelpers::info('Template cache clean. Removing PHP-DI cache...'));

        // Remove PHP-DI cache files
        // Doctrine\Cache genereert bestanden die langer zijn als toegestaan door Windows
        // Dus voor een windows enviroment kan de cache niet geleegd worden.
        if (s(PHP_OS)->toUpperCase()->startsWith('WIN')) {
            $output->writeln(ConsoleHelpers::error('Container cache Cannot be cleaned on windows.'));
        } else {
            $this->emptyDirectory(__DIR__.'/../../Storage/Cache/Container');
            $output->writeln(ConsoleHelpers::info('Container cache is clean.'));
        }

        $output->writeln(ConsoleHelpers::info('All done.'));
    }

    /**
     * Clean the given directory of any files or directories recursively.
     * Does not remove the root folder.
     *
     * @param string $path
     */
    private function emptyDirectory(string $path)
    {
        $it = new RecursiveDirectoryIterator($path, RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);

        foreach ($files as $file) {
            if ($file->isDir()) {
                rmdir(addslashes($file->getRealPath()));
            } else {
                unlink(addslashes($file->getRealPath()));
            }
        }
    }
}
