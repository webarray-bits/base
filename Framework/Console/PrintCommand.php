<?php

namespace Framework\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PrintCommand extends Command
{
    /**
     * Configure the command name and arguments.
     */
    protected function configure()
    {
        $this->setName('print')
            ->setDescription('Print the given text.')
            ->addArgument('text', InputArgument::REQUIRED, 'The text to print.');
    }

    /**
     * Run the command.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(ConsoleHelpers::error($input->getArgument('text')));
    }
}
