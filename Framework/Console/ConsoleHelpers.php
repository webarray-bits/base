<?php

namespace Framework\Console;

class ConsoleHelpers
{
    public static function info(string $in): string
    {
        return "<info>$in</info>";
    }

    public static function comment(string $in): string
    {
        return "<comment>$in</comment>";
    }

    public static function question(string $in): string
    {
        return "<question>$in</question>";
    }

    public static function error(string $in): string
    {
        return "<error>$in</error>";
    }
}
