<?php

namespace Framework\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MakeControllerCommand extends Command
{
    /**
     * Configure the command name and arguments.
     */
    protected function configure()
    {
        $this->setName('make:controller')
            ->setDescription('Make a new controller.')
            ->addArgument('name', InputArgument::REQUIRED, 'The name of the controller.');
    }

    /**
     * Run the command.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = ucfirst($input->getArgument('name')).'Controller';

        // Formatting needs to stay like this, the tabs will otherwise be in the generated file.
        $code = "<?php

namespace App\\Controllers;

use Framework\\Controller\\Controller;
use Framework\\HTTP\\Response;

class $name extends Controller
{
    /**
     * $name constructor.
     * 
     * Define middleware and inject controller dependencies here.
     */
    public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return response();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return response();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        return response();
    }

    /**
     * Display the specified resource.
     *
     * @return Response
     */
    public function show(\$key)
    {
        return response();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return Response
     */
    public function edit(\$key)
    {
        return response();
    }

    /**
     * Update the specified resource in storage.
     *
     * @return Response
     */
    public function update(\$key)
    {
        return response();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return Response
     */
    public function destroy(\$key)
    {
        return response();
    }
}";

        $file = root()."/App/Controllers/$name.php";

        // Create directory if necessary.
        mkdir(dirname($file), 0755, true);

        if (file_exists($file)) {
            $output->writeln(ConsoleHelpers::error('De controller: '.$name.' Bestaat al.'));
        } else {
            if (file_put_contents($file, $code) === false) {
                $output->writeln(ConsoleHelpers::error('De controller: '.$name.' is niet aangemaakt.'));
            } else {
                $output->writeln(ConsoleHelpers::comment('De controller: '.$name.' is succesvol aangemaakt.'));
            }
        }
    }
}
