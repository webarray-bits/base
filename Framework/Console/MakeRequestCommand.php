<?php

namespace Framework\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class MakeRequestCommand extends Command
{
    /**
     * Configure the command name and arguments.
     */
    protected function configure()
    {
        $this->setName('make:request')
            ->setDescription('Make a new request.')
            ->addArgument('name', InputArgument::REQUIRED, 'The name of the request.')
            ->addOption(
                'group',
                null,
                InputOption::VALUE_OPTIONAL,
                'The group this request will belong to.'
            );
    }

    /**
     * Run the command.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = ucfirst($input->getArgument('name')).'Request';
        $namespace = 'App\\Requests';

        // Check if a group has been specified.
        if ($input->hasOption('group')) {
            $namespace .= '\\'.ucfirst($input->getOption('group'));
        }

        // Formatting needs to stay like this, the tabs will otherwise be in the generated file.
        $code = "<?php

namespace $namespace;

use Framework\\Http\\Requests\\IRequestValidator;
use Framework\\Http\\Requests\\RequestValidator;

class $name extends RequestValidator implements IRequestValidator
{
    /**
     * This will trigger the validation.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Here the rules for validation are specified in key => value pairs.
     * The key is the name of the element in the input.
     * The value is what to validate.
     */
    public function rules(): array 
    {
        return [
            
        ];
    }
    
    /**
     * Here filters to use on elements can be specified.
     */
    public function filters(): array 
    {
        return [
            
        ];
    }
    
    /**
     * Here you can specify custom names to show in the error messages.
     */
    public function fieldNames(): array
    {
        return [
            
        ];
    }

    /**
     * Optional: create a response on succes.
     * Possible values
     * message: string
     * title: string
     * type: success or error
     * redirect: url
     */
    public function success(): array
    {
        return [
            
        ];
    }
    
    /**
     * Optional: overwrite a response on error.
     * Here custom messages are specified.
     */
    public function errors(): array
    {
        return [
            
        ];
    }
}";

        if (!$input->hasOption('group')) {
            $file = root()."/App/Requests/$name.php";
        } else {
            $file = root().'/App/Requests/'.ucfirst($input->getOption('group'))."/$name.php";
        }

        if (file_exists($file)) {
            $output->writeln(ConsoleHelpers::error($name.' already exists.'));
        } else {
            if (file_put_contents($file, $code) === false) {
                $output->writeln(ConsoleHelpers::error($name.' could not be created.'));
            } else {
                $output->writeln(ConsoleHelpers::comment($name.' created.'));
            }
        }
    }
}
