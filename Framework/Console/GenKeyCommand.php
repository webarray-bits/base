<?php

namespace Framework\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use function Stringy\create as s;

class GenKeyCommand extends Command
{
    /**
     * Configure the command name and arguments.
     */
    protected function configure()
    {
        $this->setName('gen:key')
                ->setDescription('Generate a random application key.');
    }

    /**
     * Run the command.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = __DIR__.'/../../App/Config/app.ini';

        // Get the app.ini file as an array
        $configFile = file($path);

        // openssl_random_pseudo_bytes does necessarily produce readable glyphs.
        // An md5 hash does, so we use that.
        $newKey = md5(openssl_random_pseudo_bytes(200));

        // Iterate over the lines in the config file to find the 'Key' string.
        // If it is found, place the new application key there and write the file.
        // If it is not found, notify the user that they need to place this index in the app.ini file.
        // We can't do this for them, because of ini-file sections.
        for ($i = 0; $i < sizeof($configFile); ++$i) {
            if (s($configFile[$i])->startsWith('Key')) {
                $configFile[$i] = "Key = $newKey".PHP_EOL;
                file_put_contents($path, $configFile);
                $output->writeln(ConsoleHelpers::info('Successfully generated and set the app key.'));

                return;
            }
        }

        $output->writeln("<error>Could not find the index 'Key' in the file 'App/Config/app.ini'. Please add this index to the file. It should not be contained within a section. Please note that the index is capital sensitive.</error>");
    }
}
