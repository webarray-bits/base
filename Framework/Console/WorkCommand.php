<?php

namespace Framework\Console;

use Psy\Configuration;
use Psy\Shell;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class WorkCommand extends Command
{
    /**
     * Configure the command name and arguments.
     */
    protected function configure()
    {
        $this->setName('work')
                ->setDescription('Interact with the application via the console');
    }

    /**
     * Run the command.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $config = new Configuration();
        $config->setDefaultIncludes([root().'/vendor/autoload.php']);

        $shell = new Shell();
        $shell->run();
    }
}
