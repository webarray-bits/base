<?php

namespace Framework;

use Framework\Util\DotArray;
use function Stringy\create as s;

class Application
{
    /**
     * The configuration for this application instance.
     *
     * This is initialized with an empty string, because
     * a reference to an empty array is null and DotArray::set wants
     * a reference to this array.
     *
     * @var array
     */
    protected $config = null;

    /**
     * The configuration path.
     *
     * @var string
     */
    protected $configPath = 'App/Config';

    /**
     * @var Kernel
     */
    protected $kernel;

    /**
     * @var bool
     */
    protected $inConsoleMode = false;

    /**
     * The absolute root path of this application.
     *
     * @var string
     */
    protected $rootPath = null;

    /**
     * Start the application.
     */
    public function boot()
    {
        // If no root path has been given, try to find one.
        if ($this->rootPath === null) {
            if (!$this->inConsoleMode) {
                $this->rootPath = $_SERVER['DOCUMENT_ROOT'];
            } else {
                // We don't know: best bet is getcwd() :(
                $this->rootPath = getcwd();
            }
        }

        // Make sure the root path has a trailing slash.
        $this->rootPath = s($this->rootPath)->ensureRight('/');

        /*
         * If a configuration has been given, we don't need to load it.
         * Otherwise; load it!
         */
        if ($this->config === null) {
            $this->config = Config::load($this->rootPath, $this->configPath);
        }

        // App.Path should always refer to the root application path.
        $this->config('App.Path', (string) $this->rootPath);

        /*
         * With the configuration loaded, the dependency container
         * can be initialized. The current application instance is set as 'app'.
         */
        Container::init($this);
        Container::set('app', $this);

        $this->kernel = new Kernel();
        $this->kernel->start();
    }

    /**
     * Stop the application.
     */
    public function stop()
    {
        $this->config = [];
        $this->kernel = null;

        Container::empty();
    }

    /**
     * Reboot the application.
     */
    public function reboot()
    {
        $this->stop();
        $this->boot();
    }

    /**
     * If only a key is given, retrieve a configuration item.
     * If a key and value are given, set a configuration item.
     *
     * @param string $key
     * @param null   $value
     *
     * @return array|mixed|null
     */
    public function config(string $key, $value = null)
    {
        if ($value === null) {
            return DotArray::get($this->config, $key);
        } else {
            DotArray::set($this->config, $key, $value);
        }
    }

    /**
     * @param array $config
     */
    public function setConfiguration(array $config)
    {
        $this->config = $config;
    }

    /**
     * Set the configuration directory path.
     * The configuration array has a higher priority than the directory.
     * If no path is set, 'App/Config' is assumed as the config directory path.
     *
     * @param string $configPath
     */
    public function setConfigurationDirectory(string $configPath)
    {
        $this->configPath = $configPath;
    }

    /**
     * @return string
     */
    public function getConfigurationDirectory()
    {
        return $this->configPath;
    }

    /**
     * @param bool $inConsoleMode
     */
    public function setConsoleMode(bool $inConsoleMode)
    {
        $this->inConsoleMode = $inConsoleMode;
    }

    /**
     * @return bool
     */
    public function inConsoleMode()
    {
        return $this->inConsoleMode;
    }

    /**
     * Set the root path ('/') for the application.
     *
     * @param string $rootPath
     */
    public function setRootPath(string $rootPath)
    {
        $this->rootPath = $rootPath;
    }

    /**
     * @return string
     */
    public function getRootPath()
    {
        return $this->rootPath;
    }
}
