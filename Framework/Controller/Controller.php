<?php

namespace Framework\Controller;

use Framework\Middleware\Middleware;

class Controller
{
    /**
     * @var array
     */
    private $middleware = [];

    protected function addMiddleware($instance, array $options = [])
    {
        // Apply on all routes.
        if ($options === []) {
            $this->middleware[] = ['instance' => $instance];
        }

        // Apply only on given routes.
        if (array_key_exists('on', $options)) {
            // On should always be an array.
            if (is_string($options['on'])) {
                $options['on'] = [$options['on']];
            }

            $this->middleware[] = ['instance' => $instance, 'on' => $options['on']];
        }
    }

    /**
     * Run all middleware that has been defined on the controller.
     *
     * @return bool
     *
     * @throws \Whoops\Exception\ErrorException
     */
    public function runMiddleware(): bool
    {
        // The method that is called on the controller.
        $controllerAction = request()->action();

        foreach ($this->middleware as $definition) {
            // Check if this middleware is applicable to this request.
            if (array_key_exists('on', $definition)
            && !in_array($controllerAction, $definition['on'])) {
                continue;
            }

            switch ($definition['instance']->run()) {
                case Middleware::Success:
                    continue;
                break;

                case Middleware::Abort:
                    return false;
                break;

                default:
                    throw new \Exception('Invalid Middleware result received! Middleware should return a result.');
                break;
            }
        }

        return true;
    }
}
