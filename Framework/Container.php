<?php

namespace Framework;

use DI\ContainerBuilder;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\Common\Cache\FilesystemCache;
use Doctrine\Common\Cache\RedisCache;
use Framework\Http\Request;
use Invoker\Reflection\CallableReflection;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;

class Container
{
    /**
     * @var \DI\Container
     */
    private static $container;

    /**
     * Initialize the PHP-DI container.
     *
     * @param Application $app
     */
    public static function init(Application $app)
    {
        $builder = new ContainerBuilder();

        $builder
            ->useAutowiring(true)
            ->useAnnotations(false);

        // Set PHP-DI caching if defined.
        $cachingMethod = $app->config('Cache.PHP-DI.Storage');
        $appKey = $app->config('App.Key');

        if ($cachingMethod == 'Array') {
            $cache = new ArrayCache();
            $cache->setNamespace($appKey);
            $builder->setDefinitionCache($cache);
        } elseif ($cachingMethod == 'File') {
            $cache = new FilesystemCache(root() . '/Storage/Cache/Container');
            $cache->setNamespace($appKey);
            $builder->setDefinitionCache($cache);
        } elseif ($cachingMethod == 'Redis') {
            $cache = new RedisCache();
            $cache->setNamespace($appKey);
            $builder->setDefinitionCache($cache);
        }

        self::$container = $app->config('App.Production', true)
                        ? $builder->build()
                        : $builder->buildDevContainer();
    }

    /**
     * Set a class within the PHP-DI container.
     *
     * @param string $name
     * @param mixed  $value
     *
     * @return mixed
     */
    public static function set(string $name, $value)
    {
        self::$container->set($name, $value);

        return self::get($name);
    }

    /**
     * Resolve a class out of the PHP-DI container.
     *
     * @param string $name
     *
     * @return mixed
     */
    public static function get(string $name)
    {
        return self::$container->get($name);
    }

    /**
     * Call a Closure and inject dependencies.
     *
     * @param callable $callable
     * @param array    $arguments
     *
     * @return mixed
     */
    public static function call(callable $callable, array $arguments = [])
    {
        $reflector = CallableReflection::create($callable);
        $parameters = $reflector->getParameters();
        $modelTypeHints = [];

        foreach ($parameters as $parameter) {
            /*
             * When no typehints are set for the ReflectionParameter, getType will return null.
             */
            if ($parameter->getType() === null) {
                continue;
            }

            /*
             * We check for class existance, because 'int' and other scalars are valid typehints, but they are not
             * considered a class.
             */
            if (!class_exists($parameter->typeHint)) {
                continue;
            }

            /*
             * Propel models implement the ActiveRecordInterface.
             */
            if (in_array(ActiveRecordInterface::class, $class->getInterfaceNames())) {
                /*
                 * Build the query and try to retrieve the object.
                 * The name of the property to filter by is the name of the typehint.
                 * So the typehint App\Models\NewsItem $slug would retrieve the NewsItem by its slug.
                 */
                $queryClass = $class->getName() . 'Query';
                $query = call_user_func([$queryClass, 'create']);
                $modelTypeHints[$parameter->name] = $query->{'findOneBy'.ucfirst($parameter->name)}($arguments[$parameter->name]);
            }
        }

         return self::$container->call($callable, array_merge($arguments, $modelTypeHints));
    }

    /**
     * Empty en rebuild the container.
     */
    public static function empty()
    {
        unset(self::$container);
        self::init();
    }

    /**
     * @return \DI\Container
     */
    public static function getInstance()
    {
        return self::$container;
    }
}
