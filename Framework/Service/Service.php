<?php

namespace Framework\Service;

abstract class Service
{
    /**
     * All event listeners that are registered by this service.
     * Here, the key should be a string containing the event name.
     * The value should be a callable or an instance of EventSubscriberInterface.
     *
     * @var array
     */
    protected $listeners = [];

    /**
     * All middleware that is defined by this service.
     *
     * @var array
     */
    protected $middleware = [];

    /**
     * All routes that are defined by this service.
     * This should only contain Framework\Http\Route instances.
     *
     * @var array
     */
    protected $routes = [];

    /**
     * All validators defined by this service.
     * This should only contain callables.
     *
     * @var array
     */
    protected $validators = [];

    /**
     * All Twig extensions defined by this service.
     * This should only contain Twig_Extensions instances.
     *
     * @var array
     */
    protected $extensions = [];

    /**
     * All helper functions defined by this service.
     * The helpers are in name => callable pairs.
     *
     * @var array
     */
    protected $helpers = [];

    /**
     * The registrar in which this service is registered.
     *
     * @var ServiceRegistrar
     */
    protected $registrar;

    /**
     * Retrieve the name of the service.
     *
     * @return string
     */
    abstract public function name(): string;

    /**
     * Helper function for clean listener definitions.
     *
     * @return array
     */
    protected function listeners(): array
    {
        return [];
    }

    /**
     * Helper function for clean middleware definitions.
     *
     * @return array
     */
    protected function middleware(): array
    {
        return [];
    }

    /**
     * Helper function for clean route definitions.
     *
     * @return array
     */
    protected function routes(): array
    {
        return [];
    }

    /**
     * @return array
     */
    public function getHelpers(): array
    {
        return $this->helpers;
    }

    /**
     * Helper function for clean helpers definitions.
     *
     * @return array
     */
    protected function helpers(): array
    {
        return [];
    }

    /**
     * Helper function for clean validator definitions.
     *
     * @return array
     */
    protected function validators(): array
    {
        return [];
    }

    /**
     * This function can be used to check if the service can be started.
     * For example: the SessionService checks if the application is in console mode.
     * If it is, there is no use in initialising the service.
     *
     * @return bool
     */
    protected function canStart(): bool
    {
        return true;
    }

    /**
     * Helper function for clean dependency definitions.
     *
     * @return array
     */
    public function dependencies(): array
    {
        return [];
    }

    /**
     * Initializes the service.
     *
     * @internal
     * @param ServiceRegistrar $registrar
     */
    public function initialize(ServiceRegistrar $registrar)
    {
        if (!$this->canStart()) {
            return;
        }

        $this->registrar = $registrar;

        $this->listeners = $this->listeners();
        $this->middleware = $this->middleware();
        $this->routes = $this->routes();
        $this->validators = $this->validators();
        $this->helpers = $this->helpers();
    }

    /**
     * @return array
     */
    public function getValidators(): array
    {
        return $this->validators;
    }

    /**
     * @return array
     */
    public function getListeners(): array
    {
        return $this->listeners;
    }

    /**
     * @return array
     */
    public function getMiddleware(): array
    {
        return $this->middleware;
    }

    /**
     * @return array
     */
    public function getRoutes(): array
    {
        return $this->routes;
    }

    /**
     * @param array $listeners
     */
    protected function setListeners(array $listeners)
    {
        $this->listeners = $listeners;
    }

    /**
     * @param array $middleware
     */
    protected function setMiddleware(array $middleware)
    {
        $this->middleware = $middleware;
    }

    /**
     * @param array $routes
     */
    protected function setRoutes(array $routes)
    {
        $this->routes = $routes;
    }

    /**
     * @param array $validators
     */
    protected function setValidators(array $validators)
    {
        $this->validators = $validators;
    }

    /**
     * @param array $helpers
     */
    protected function setHelpers(array $helpers)
    {
        $this->helpers = $helpers;
    }
}