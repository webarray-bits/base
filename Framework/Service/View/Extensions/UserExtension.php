<?php

namespace Framework\Service\View\Extensions;

use Framework\Facades\User;
use Twig_SimpleFunction;

class UserExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{
    public function getName()
    {
        return 'UserExtension';
    }

    public function getGlobals()
    {
        return [
            'user' => User::get(),
        ];
    }

    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction('auth', [$this, 'authFunction']),
        ];
    }

    /**
     * If no roleName is given, check if a user is logged in.
     * Otherwise, check if the user has that role.
     *
     * @param $roles
     *
     * @return bool
     */
    public function authFunction(...$roles)
    {
        // If the user is not logged in we automatically fail.
        if (!User::loggedIn()) {
            return false;
        }

        // If no roles are specified, we are done.
        if (sizeof($roles) === 0) {
            return true;
        }

        // If the user has one of the specified roles, return true.
        foreach ($roles as $role) {
            if (User::hasRole($role)) {
                return true;
            }
        }

        // Not authorized.
        return false;
    }
}
