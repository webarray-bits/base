<?php

namespace Framework\Service\View\Extensions;

use Carbon\Carbon;
use Framework\Config;
use Twig_SimpleFilter;
use Twig_SimpleFunction;
use Framework\Container;
use Framework\Facades\Session;
use Framework\Http\RouteRegistry;

class GlobalExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{
    public function getName()
    {
        return 'Extension';
    }

    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction('action', [$this, 'actionFunction']),
            new Twig_SimpleFunction('old', [$this, 'oldFunction']),
            new Twig_SimpleFunction('config', [$this, 'configFunction']),
            new Twig_SimpleFunction('array_values', 'array_values'),
            new Twig_SimpleFunction('array_keys', 'array_keys'),
            new Twig_SimpleFunction('json_encode', 'json_encode'),
            new Twig_SimpleFunction('json_decode', 'json_decode'),
        ];
    }

    public function getFilters()
    {
        return [
            new Twig_SimpleFilter('humanDate', [$this, 'humanDateFilter']),
            new Twig_SimpleFilter('words', [$this, 'wordsFilter']),
        ];
    }

    public function actionFunction($action, $replacement = null)
    {
        return Container::get(RouteRegistry::class)->getActionUrl($action, $replacement);
    }

    public function oldFunction($key)
    {
        return Session::get('last_input.'.$key, '');
    }

    public function configFunction($key)
    {
        return config($key);
    }

    public function humanDateFilter(\DateTime $date, string $format = 'Y-m-d H:i:s')
    {
        return (new Carbon($date->format($format)))->diffForHumans();
    }

    public function wordsFilter(string $string, int $words)
    {
        $text = preg_split("/[\s,]+/", strip_tags($string));

        if (sizeof($text) < $words) {
            return $string;
        }

        return htmlspecialchars_decode(
            implode(' ', array_slice(
                $text,
                0,
                $words)
            )
        );
    }

    public function __destruct()
    {
        /*
         * Unset the last input, but only when the $_SESSION exists.
         * The $_SESSION does not exist in console mode.
         */
        if (isset($_SESSION)) {
            unset($_SESSION['last_input']);
        }
    }
}
