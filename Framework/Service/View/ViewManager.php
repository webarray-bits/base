<?php

namespace Framework\Service\View;

use Framework\Config;
use Framework\Util\Form;
use Twig_ExtensionInterface;

class ViewManager
{
    /**
     * @var \Twig_Loader_Filesystem
     */
    protected $loader;

    /**
     * @var \Twig_Environment
     */
    protected $twig;

    public function __construct()
    {
        $options = [];

        if (config('Cache.Templates')) {
            $options['cache'] = config('Cache.TemplateDir');
        }

        if (!config('App.Production')) {
            $options['debug'] = true;
        }

        // We use the filesystem loader for now
        $this->loader = new \Twig_Loader_Filesystem(config('Twig.Dir'));

        // We create the Twig object
        $this->twig = new \Twig_Environment($this->loader, $options);
        $this->twig->addExtension(new \Twig_Extension_Optimizer(-1));

        foreach (config('Twig.Extensions') as $ext) {
            $this->twig->addExtension(new $ext());
        }

        if (!config('App.Production')) {
            $this->twig->addExtension(new \Twig_Extension_Debug());
        }

        $this->twig->addGlobal('form', new Form());
    }

    /**
     * @param \Twig_SimpleFunction $function
     */
    public function addFunction(\Twig_SimpleFunction $function)
    {
        $this->twig->addFunction($function);
    }

    /**
     * @param array $functions
     */
    public function addFunctions(array $functions)
    {
        foreach ($functions as $function) {
            $this->addFunction($function);
        }
    }

    /**
     * @param \Twig_SimpleFilter $filter
     */
    public function addFilter(\Twig_SimpleFilter $filter)
    {
        $this->twig->addFilter($filter);
    }

    /**
     * @param array $filters
     */
    public function addFilters(array $filters)
    {
        foreach ($filters as $filter) {
            $this->addFilter($filter);
        }
    }

    /**
     * @param Twig_ExtensionInterface $extension
     */
    public function addExtension(Twig_ExtensionInterface $extension)
    {
        $this->twig->addExtension($extension);
    }

    /**
     * @param array $extensions
     */
    public function addExtensions(array $extensions)
    {
        foreach ($extensions as $extension) {
            $this->addExtension($extension);
        }
    }

    /**
     * Render's a .html file using the Twig templating engine.
     *
     * @param string $name
     * @param array  $vars
     *
     * @return string
     */
    public function render(string $name, array $vars = []): string
    {
        $name = str_replace('.', '/', $name).'.html';

        return $this->twig->render($name, $vars);
    }

    /*
     * Loads an error page out of /Resources/Error, extension should be .html.
     * @param string $name
     * @return string
     */
    public function error(string $name): string
    {
        $name = str_replace('.', '/', (string) $name).'.html';

        return file_get_contents(root().'/Resources/Error/'.$name);
    }

    public function customError(string $title, string $content): string
    {
        return $this->twig->createTemplate(
            file_get_contents(root().'/Resources/Error/Template.html')
        )->render(['title' => $title, 'content' => $content]);
    }
}
