<?php

namespace Framework\Service\View;

use Framework\Container;
use Framework\Event\Kernel\KernelStartEvent;
use Framework\Http\Response;
use Framework\Service\Service;
use Framework\Service\View\Extensions\GlobalExtension;
use Framework\Service\View\Extensions\UserExtension;

class ViewService extends Service
{

    /**
     * Retrieve the name of the service.
     *
     * @return string
     */
    public function name(): string
    {
        return 'view';
    }

    public function listeners(): array
    {
        return [
            'kernel.start'  => function(KernelStartEvent $event) {
                app(ViewManager::class)
                    ->addExtension(new GlobalExtension());

                app(Response::class)
                    ->registerExtension('view', function (string $name, array $variables = []): Response {
                        return app(Response::class)->setContent(
                            app(ViewManager::class)->render($name, $variables)
                        );
                    });
            }
        ];
    }

    public function helpers(): array
    {
        return [
            /*
             * Shortcut method for rendering a view.
             * If no arguments are given, the ViewManager instance is returned.
             *
             * @param string $name
             * @param array  $vars
             *
             * @return string|\Framework\Views\ViewManager
             */
            'view' => function ($name = null, array $vars = [])
            {
                if ($name === null) {
                    return app(ViewManager::class);
                }

                return app(ViewManager::class)->render($name, $vars);
            }
        ];
    }
}