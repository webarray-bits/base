<?php

namespace Framework\Service;

use BideoWego\FuncFactory;
use Framework\Container;
use Framework\Http\Requests\DynamicValidator;
use Framework\Http\RouteRegistry;
use Framework\Middleware\Proxy;
use MJS\TopSort\Implementations\StringSort;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * The ServiceRegistrar manages all services.
 * It handles the initialization, loading and event handling.
 *
 * Class ServiceRegistrar
 */
class ServiceRegistrar
{
    /**
     * This is an array containing all services defined in the configuration of the application.
     * The class strings are retrieved from config.
     *
     * @var string[]
     */
    protected $classes = [];

    /**
     * Array containing the instances of the classes
     * that are defined in $classes.
     *
     * @var Service[]
     */
    protected $instances = [];

    /**
     * @var EventDispatcher
     */
    protected $dispatcher;

    /**
     * @var DynamicValidator
     */
    protected $validator;

    /**
     * The RouteRegistry instance for the application.
     * This is used to register all routes that services can define.
     *
     * @var RouteRegistry
     */
    protected $registry;

    /**
     * ServiceRegistrar constructor.
     */
    public function __construct()
    {
        // Retrieve all Service classes from the configuration.
        $this->classes = config('App.Services');

        // Assign the field injected by the service container.
        $this->dispatcher = Container::get(EventDispatcher::class);
        $this->registry = Container::get(RouteRegistry::class);
        $this->validator = Container::get(DynamicValidator::class);
    }

    /**
     * Load all services.
     *
     * @internal
     */
    public function initialize()
    {
        // If no services are defined, end.
        if ($this->classes === []) {
            return;
        }

        // Remove duplicate definitions.
        $this->classes = array_unique($this->classes);

        /**
         * @var Service[]
         */
        $instances = [];

        // Create all instances.
        foreach ($this->classes as $class) {
            $instance = Container::get($class);
            $instances[$instance->name()] = $instance;
        }

        /**
         * Topological sorter for dependency resolution.
         *
         * @var StringSort
         */
        $sorter = new StringSort();

        foreach ($instances as $instance) {
            $sorter->add($instance->name(), $instance->dependencies());
        }

        /*
         * The sorter either throws an error about a missing dependency or circular dependencies,
         * or returns an array with service names in the order that they should be initialized.
         */
        foreach ($sorter->sort() as $name) {
            $instances[$name]->initialize($this);
        }

        $this->instances = $instances;

        /*
         * First, we need to register the middleware.
         * After this all listeners and routes can be defined.
         */
        foreach ($this->instances as $instance) {
            $this->registerMiddleware($instance);
            $this->registerHelpers($instance);
        }

        foreach ($this->instances as $instance) {
            $this->registerListeners($instance);
            $this->registerRoutes($instance);
            $this->registerValidators($instance);
        }
    }

    /**
     * @param string $name
     * @param Event  $event
     */
    public function dispatch(string $name, Event $event)
    {
        $this->dispatcher->dispatch($name, $event);
    }

    /**
     * @param string $name
     * @param $listener
     * @param int $priority
     */
    public function listen(string $name, $listener, int $priority = 0)
    {
        $this->registerListenerHelper($name, $listener, $priority);
    }

    public function serviceDefined(string $name): bool
    {
        return isset($this->instances[$name]);
    }

    /**
     * @param Service $instance
     */
    protected function registerListeners(Service $instance)
    {
        foreach ($instance->getListeners() as $name => $listener) {
            $this->registerListenerHelper($name, $listener);
        }
    }

    /**
     * @param $name
     * @param $listener
     * @param int $priority
     */
    private function registerListenerHelper($name, $listener, int $priority = 0)
    {
        if (is_array($listener)) {
            foreach ($listener as $sub) {
                $this->registerListenerHelper($sub);
            }
        } elseif (is_callable($listener)) {
            $this->dispatcher->addListener($name, $listener, $priority);
        } elseif ($listener instanceof EventSubscriberInterface) {
            $this->dispatcher->addSubscriber($listener);
        }
    }

    /**
     * @param Service $instance
     */
    protected function registerMiddleware(Service $instance)
    {
        foreach ($instance->getMiddleware() as $name => $class) {
            /*
             * Class definitions are saved in the Middleware\Proxy class.
             * They are resolved on middleware instantiation.
             */
            Proxy::$definitions[$name] = $class;
        }
    }

    /**
     * @param Service $instance
     */
    protected function registerValidators(Service $instance)
    {
        foreach ($instance->getValidators() as $name => $validator) {
            $this->validator->registerExtension($name, $validator);
        }
    }

    /**
     * @param Service $instance
     */
    protected function registerRoutes(Service $instance)
    {
        foreach ($instance->getRoutes() as $route) {
            $this->registry->register($route);
        }
    }

    /**
     * @param Service $instance
     */
    protected function registerHelpers(Service $instance)
    {
        foreach ($instance->getHelpers() as $name => $function) {
            FuncFactory::create($name, $function);
        }
    }
}