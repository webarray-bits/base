<?php

namespace Framework\Service\Session;

use Framework\Container;
use Framework\Event\Kernel\KernelStartEvent;
use Framework\Service\Service;
use Framework\Service\View\ViewManager;

class SessionService extends Service
{
    /**
     * Retrieve the name of the service.
     *
     * @return string
     */
    public function name(): string
    {
        return 'session';
    }

    public function canStart(): bool
    {
        return !app()->inConsoleMode();
    }

    public function listeners(): array
    {
        return [
            'kernel.start' => function (KernelStartEvent $event) {
                session_start();

                if ($this->registrar->serviceDefined('view')) {
                    Container::get(ViewManager::class)
                        ->addFunction(new \Twig_SimpleFunction('session', ['session', 'session']));
                }
            }
        ];
    }

    public function helpers(): array
    {
        return [
            'session' => function($key = null, $default = null) {
                if ($key === null) {
                    return Container::get(SessionManager::class);
                }

                return Container::get(SessionManager::class)->get($key, $default);
            }
        ];
    }
}