<?php

namespace Framework\Service\Session;

use Framework\Util\DotArray;

class SessionManager
{
    public function get(string $key, $default = null)
    {
        return DotArray::get($_SESSION, $key, $default);
    }

    public function getArray()
    {
        return $_SESSION;
    }

    public function pull(string $key, $default = null)
    {
        return DotArray::pull($_SESSION, $key, $default);
    }

    public function set(string $key, $value)
    {
        DotArray::set($_SESSION, $key, $value);
    }

    public function add(string $key, $value)
    {
        DotArray::add($_SESSION, $key, $value);
    }

    public function forget(string $key)
    {
        DotArray::forget($_SESSION, $key);
    }

    public function exists(string $key):bool
    {
        return DotArray::exists($_SESSION, $key);
    }
}