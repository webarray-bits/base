<?php

namespace Framework\Service\Database;

use Framework\Service\Service;

class DatabaseService extends Service
{
    /**
     * Retrieve the name of the service.
     *
     * @return string
     */
    public function name(): string
    {
        return 'database';
    }

    /**
     * Helper function for clean listener definitions.
     *
     * @return array
     */
    protected function listeners(): array
    {
        return [
            new DatabaseLoader(),
        ];
    }

    protected function validators(): array
    {
        return [
            'validate_exists' => [DatabaseValidators::class, 'validate_exists'],
            'validate_unique' => [DatabaseValidators::class, 'validate_unique'],
        ];
    }
}
