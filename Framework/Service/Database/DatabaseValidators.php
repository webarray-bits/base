<?php

namespace Framework\Service\Database;

class DatabaseValidators
{
    /**
     * This will validate whether an entry exists within the database.
     * The most basic usage of the filter is as follows:.
     *
     * 'id' => 'exists,events'
     *
     * Which checks if a row exists with the given id in the events table.
     * More advanced filtering can be applied:
     *
     * 'EventId' => 'exists,event:id'
     *
     * Here the :id is used to specify the column name. Without it, EventId would be used as the column name.
     *
     * 'EventId' => 'exists,event:id:archived:false'
     *
     * Here a check is applied: the archived column on the table must be false (or falsey) in order for the
     * validation to succeed. This is helpful in situations where archival or soft deletion is used.
     * This check works with all data types.
     *
     * Please note that the column name has to be supplied when using the filter.
     *
     * @param $field
     * @param $input
     * @param null $param
     *
     * @return array|null
     *
     * @throws \Exception
     */
    public static function validate_exists($field, $input, $param = null)
    {
        if ($param === null) {
            throw new \Exception('Exists parameter cannot be empty.');
        }

        if (!isset($input[$field]) || empty($input[$field])) {
            return null;
        }

        $params = explode(':', $param);
        $class = '\\App\\Models\\'.(string) ucfirst($params[0]).'Query';
        $column = ucfirst($params[1] ?? $field);
        $function = "findOneBy$column";

        // With one or two parameter(s), a simple check is requested.
        if (sizeof($params) <= 2) {
            // Check if the row exists.
            if ($class::create()->{$function}($input[$field]) === null) {
                return [
                    'field' => $field,
                    'value' => $input[$field],
                    'rule' => __FUNCTION__,
                    'param' => $param,
                ];
            }

            // Validation succeeded
            return;
        }

        // Four parameters specified, so a check with a condition is requested.
        if (sizeof($params) === 3) {
            throw new \Exception('Not enough parameters are given for the exists parameter.');
        }

        /*
         * Check if the request filter method exists.
         *
         * Boolean field filters use 'Is' before the name of the column,
         * so we check for this.
         */
        $proxy = $class::create();
        $filter = 'filterBy';

        // Check for method without 'Is'.
        if (method_exists($proxy, "$filter$column")) {
            $filter .= $column;
        }

        // Check for method with 'Is'.
        elseif (method_exists($proxy, "$filter"."Is$column")) {
            $filter .= "Is$column";
        }

        // Filter does not exist for $field.
        else {
            throw new \Exception("No filter exists for the column $column.");
        }

        // Convert the parameter to the right type.
        $check = self::string_to_type($params[3]);

        // Execute the check.
        if ($proxy->{$filter}($check)->{$function}($input[$field]) === null) {
            return [
                'field' => $field,
                'value' => $input[$field],
                'rule' => __FUNCTION__,
                'param' => $param,
            ];
        }
    }

    /**
     * This will validate is a column is unique.
     * The name of the input must be same as the phpName of the column.
     * The argument given is the name of the model.
     *
     * Example:
     * 'Username' => 'required|unique,User'
     *
     * @param $field
     * @param $input
     * @param null $param
     *
     * @return array|null
     *
     * @throws \Exception
     */
    public function validate_unique($field, $input, $param = null)
    {
        if ($param === null) {
            throw new \Exception('Unique parameter cannot be empty');
        }

        if (!isset($input[$field]) || empty($input[$field])) {
            return null;
        }

        $params = explode(':', $param);

        $class = '\\App\\Models\\'.(string) ucfirst($params[0]).'Query';

        $field = $params[1] ?? $field;

        $function = 'findOneBy'.$field;

        if ($class::create()->{$function}($input[$field]) !== null) {
            return [
                'field' => $field,
                'value' => $input[$field],
                'rule' => __FUNCTION__,
                'param' => $param,
            ];
        }
    }

    /**
     * @param $var
     *
     * @return int|mixed|string
     */
    private static function string_to_type($var)
    {
        // Boolean
        if (in_array($var, ['true', 'false', '1', '0', 'yes', 'no'], true)) {
            return filter_var($var, FILTER_VALIDATE_BOOLEAN);
        }

        // Integer or floating point
        elseif (is_numeric($var)) {
            return $var + 0;
        } // Trick to convert number to integer or float.

        // String
        return $var;
    }
}
