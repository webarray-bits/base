<?php

namespace Framework\Service\CSRF;

use Framework\Container;
use Framework\Event\Http\HttpDispatchEvent;
use Framework\Event\Kernel\KernelStartEvent;
use Framework\Service\Service;
use Framework\Service\View\ViewManager;

class CSRFService extends Service
{
    const TOKEN_ENTRY_NAME = 'csrf_token';

    /**
     * Retrieve the name of the service.
     *
     * @return string
     */
    public function name(): string
    {
        return 'csrf';
    }

    public function canStart(): bool
    {
        return !app()->inConsoleMode();
    }

    public function dependencies(): array
    {
        return [
            'session'
        ];
    }

    public function helpers(): array
    {
        return [
            'csrf_token' => function() {
                return $_SESSION[self::TOKEN_ENTRY_NAME];
            }
        ];
    }

    public function listeners(): array
    {
        return [
            'kernel.start'  => function (KernelStartEvent $event) {
                if (empty($_SESSION[self::TOKEN_ENTRY_NAME])) {
                    $_SESSION[self::TOKEN_ENTRY_NAME] = bin2hex(random_bytes(32));
                }

                if ($this->registrar->serviceDefined('view')) {
                    Container::get(ViewManager::class)
                        ->addFunction(new \Twig_SimpleFunction('csrf_token', ['csrf_token', 'csrf_token']));
                }
            },
            'http.dispatch' => function (HttpDispatchEvent $event) {
                if ($event->route->method !== 'GET' && $event->route->method !== 'HEAD') {
                    return;
                }

                if (!hash_equals(input(self::TOKEN_ENTRY_NAME), $_SESSION[self::TOKEN_ENTRY_NAME])) {
                    throw new \Exception('CSRF token mismatch.');
                }
            }
        ];
    }
}