<?php

namespace Framework\Service\Cache;

use phpFastCache\CacheManager as CM;

class CacheManager implements ArrayMutable
{
    /**
     * The cache proxy object.
     *
     * @var \phpFastCache\Core\DriverAbstract
     */
    private $cache;

    /**
     * FastCacheManager constructor.
     */
    public function __construct()
    {
        CM::setup(
            config('Cache.FastCache')
        );

        $this->cache = CM::getInstance();
    }

    /**
     * Insert an item into the cache.
     *
     * @param string $key
     * @param $value
     * @param int $seconds
     */
    public function set(string $key, $value, int $seconds = 600)
    {
        // Storing An Item In The Cache
        $this->cache->set($key, $value, $seconds);
    }

    /**
     * Insert an item into the cache and remember it for 5 years.
     *
     * @param string $key
     * @param $value
     */
    public function forever(string $key, $value)
    {
        $this->set($key, $value, years(5));
    }

    /**
     * Returns whether an item exists in the cache.
     *
     * @param string $key
     *
     * @return bool
     */
    public function has(string $key): bool
    {
        // Checking For Existence In Cache
        return $this->cache->isExisting($key);
    }

    /**
     * Retrieve an item from the cache.
     *
     * @param string $key
     * @param null   $default
     *
     * @return mixed|null
     */
    public function get(string $key, $default = null)
    {
        return $this->cache->get($key) ?? $default;
    }

    /**
     * Retrieve an item from the cache, and then delete it.
     *
     * @param string $key
     *
     * @return mixed|null
     */
    public function pull(string $key)
    {
        $value = $this->get($key);
        $this->forget($key);

        return $value;
    }

    /**
     * Delete an item from the cache.
     *
     * @param string $key
     */
    public function forget(string $key)
    {
        $this->cache->delete($key);
    }

    /**
     * Insert an item into the cache and remember it for $minutes.
     *
     * @param string   $key
     * @param int      $seconds
     * @param callable $function
     *
     * @return mixed|null
     */
    public function remember(string $key, int $seconds, callable $function)
    {
        if (!$this->has($key)) {
            $this->set($key, $function(), $seconds);
        }

        return $this->get($key);
    }

    /**
     * Insert an item into the cache and remembers it for 5 years.
     *
     * @param string   $key
     * @param callable $function
     *
     * @return mixed|null
     */
    public function rememberForever(string $key, callable $function)
    {
        return $this->remember($key, years(5), $function);
    }

    /**
     * Clean the cache.
     */
    public function clean()
    {
        $this->cache->clean();
    }

    /**
     * Returns cache statistics.
     *
     * @return array
     */
    public function stats() :array
    {
        return $this->cache->stats();
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function exists(string $key):bool
    {
        return $this->cache->isExisting($key);
    }
}