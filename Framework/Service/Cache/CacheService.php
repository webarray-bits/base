<?php

namespace Framework\Service\Cache;

use Framework\Service\Service;

class CacheService extends Service
{

    /**
     * Retrieve the name of the service.
     *
     * @return string
     */
    public function name(): string
    {
        return 'cache';
    }

    public function helpers(): array
    {
        return [
            /**
             * Retrieve an item from the cache.
             * If no arguments are given, returns the cache manager.
             *
             * @param string|null $key
             * @param null        $default
             *
             * @return \Framework\Data\CacheManager|mixed
             */
            'cache' => function (string $key = null, $default = null)
            {
                if ($key === null) {
                    return Container::get(CacheManager::class);
                }

                return Container::get(CacheManager::class)->get($key, $default);
            }
        ];
    }
}