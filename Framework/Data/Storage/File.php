<?php

namespace Framework\Data\Storage;

use App\Models\FileMetadata;
use App\Models\FileMetadataQuery;
use Framework\Config;
use Framework\Facades\Storage;
use Propel\Runtime\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\File as SymfonyFile;
use function Stringy\create as s;

class File extends SymfonyFile
{
    use FileShorthands;

    /**
     * The file metadata from the database.
     *
     * @var \App\Models\FileMetadata
     */
    protected $metadata;

    /**
     * Is the file deleted?
     *
     * @var bool
     */
    protected $deleted = false;

    /**
     * File constructor.
     *
     * Create a file instance by storage name or with passed metadata.
     * The storage name is the name of the file on the filesystem without the .tmp on the end.
     * The original name of the file is retrieved from the database.
     *
     * Alternatively a FileMetadata instance can be passed.
     *
     * @param string $info
     */
    public function __construct($info)
    {
        if ($info instanceof FileMetadata) {
            $this->metadata = $info;
        } else {
            /*
             * The storagename is the name of the file on the filesystem
             * without the .tmp
             */
            $this->metadata = FileMetadataQuery::create()->findOneByStorageName($info);

            if ($this->metadata === null) {
                throw new FileNotFoundException($info);
            }
        }

        $path = config('App.StorageDir').$this->metadata->getStoragePath();

        parent::__construct($path, true);
    }

    /**
     * Create a new file.
     *
     * The name is not the name that is used on the filesystem,
     * the name is used with downloading and should contain the extension.
     *
     * @param $directory
     * @param $name
     * @param string $content
     *
     * @return File
     */
    public static function create(string $directory, string $name, string $content = ''): File
    {
        $path = s($directory)->ensureRight('/').basename(tempname(sys_get_temp_dir(), ''));

        // Extract the new name from $new_path excluding the extension.
        $basename = substr(basename($path), 0, -4);

        Storage::write($path, $content);

        // Create new file metadata for the copied file.
        (new FileMetadata())
            ->setStoragePath($path)
            ->setStorageName($basename)
            ->setFileName($name)
            ->setSize(Storage::getSize($path))
            ->save();

        return new self($basename);
    }

    /**
     * Move the file.
     *
     * The directory is relative to the App.StorageDir.
     * Optionally, a new name can be given. The new name should not include the extension.
     *
     * This returns a new File instance.
     *
     * @param string $directory
     * @param null   $name
     *
     * @return File
     */
    public function move($directory, $name = null)
    {
        $file = $this->copy(
            $directory,
            $name === null
                ? $this->metadata->getStorageName()
                : $name
        );

        $this->delete();

        return $file;
    }

    /**
     * Copy the file.
     *
     * The directory is relative to the App.StorageDir.
     * Optionally, a new name can be given. The new name should not include the extension.
     *
     * This will return a File instance of the new file.
     * This will generate a new storage name for the new file.
     *
     * @param string $directory
     * @param null   $name
     *
     * @return File
     */
    public function copy($directory, $name = null)
    {
        $this->checkForDeletion();

        $new_path = s($directory)->ensureLeft('/').($name === null
                ? '/'.basename(tempnam(sys_get_temp_dir(), '')) // Generate a new random filename.
                : '/'.$name.'.tmp');

        Storage::copy(
            $this->metadata->getStoragePath(),
            $new_path
        );

        // Create new file metadata for the copied file.
        $metadata = new FileMetadata();
        $metadata
            ->setStoragePath($new_path)
            ->setStorageName(
                // Extract the new name from $new_path excluding the extension.
                substr(basename($new_path), 0, -4)
            )
            ->setSize($this->getSize())
            ->save();

        return new self($metadata);
    }

    /**
     * Delete the file.
     */
    public function delete()
    {
        $this->checkForDeletion();

        Storage::delete($this->metadata->getStoragePath());
        $this->metadata->delete();
        $this->deleted = true;
    }

    /**
     * Read the file and return its contents.
     */
    public function read()
    {
        $this->checkForDeletion();

        // Streams are used for binary file support.
        $stream = Storage::readStream($this->metadata->getStoragePath());
        $contents = stream_get_contents($stream);
        fclose($stream);

        return $contents;
    }

    /**
     * Write to the file, overwriting its contents.
     *
     * @param string $content
     */
    public function put(string $content)
    {
        $this->checkForDeletion();

        Storage::put(
            $this->metadata->getStoragePath(),
            $content
        );
    }

    /**
     * Append to the file.
     *
     * @param string $content
     */
    public function append(string $content)
    {
        // Put and read will handle the deletion check.
        $this->put(
            $this->read().$content
        );
    }

    /**
     * Get the original name as it was uploaded.
     *
     * @return string
     */
    public function getOriginalName(): string
    {
        $this->checkForDeletion();

        return $this->metadata->getFileName();
    }

    public function getStorageName(): string
    {
        $this->checkForDeletion();

        return $this->metadata->getStorageName();
    }

    /**
     * Get the directory relative to App.StorageDir.
     *
     * @return string
     */
    public function getRelativeDirectory(): string
    {
        $this->checkForDeletion();

        return dirname($this->metadata->getStoragePath());
    }

    /**
     * Get the path relative to App.StorageDir.
     *
     * @return string
     */
    public function getRelativePath(): string
    {
        $this->checkForDeletion();

        return $this->metadata->getStoragePath();
    }

    /**
     * Get the file metadata object.
     *
     * @return FileMetadata
     */
    public function getMetadata(): FileMetadata
    {
        $this->checkForDeletion();

        return $this->metadata;
    }

    /**
     * Check if the current File instance refers
     * to a deleted file.
     *
     * @throws FileNotFoundException
     */
    protected function checkForDeletion()
    {
        if ($this->deleted) {
            throw new \Exception('This file has been deleted.');
        }
    }
}
