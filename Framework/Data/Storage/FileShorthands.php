<?php

namespace Framework\Data\Storage;

trait FileShorthands
{
    public function extension()
    {
        return $this->guessExtension();
    }

    public function mimetype()
    {
        return $this->getMimeType();
    }
}
