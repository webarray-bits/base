<?php

namespace Framework\Data\Storage;

use App\Models\FileMetadata;
use Framework\Config;
use Symfony\Component\HttpFoundation\File\UploadedFile as SymfonyUploadedFile;

class UploadedFile extends SymfonyUploadedFile
{
    use FileShorthands;

    /**
     * Accepts the information of the uploaded file as provided by the PHP global $_FILES.
     *
     * The file object is only created when the uploaded file is valid (i.e. when the
     * isValid() method returns true). Otherwise the only methods that could be called
     * on an UploadedFile instance are:
     *
     *   * getClientOriginalName,
     *   * getClientMimeType,
     *   * isValid,
     *   * getError.
     *
     * Calling any other method on an non-valid instance will cause an unpredictable result.
     *
     * @param string      $path         The full temporary path to the file
     * @param string      $originalName The original file name
     * @param string|null $mimeType     The type of the file as provided by PHP; null defaults to application/octet-stream
     * @param int|null    $size         The file size
     * @param int|null    $error        The error constant of the upload (one of PHP's UPLOAD_ERR_XXX constants); null defaults to UPLOAD_ERR_OK
     * @param bool        $test         Whether the test mode is active
     *
     * @throws FileException         If file_uploads is disabled
     * @throws FileNotFoundException If the file does not exist
     */
    public function __construct($path, $originalName, $mimeType = null, $size = null, $error = null, $test = false)
    {
        parent::__construct($path, $originalName, $mimeType, $size, $error, $test);
    }

    /**
     * Store the uploaded file on the configured filesystem with a unique name.
     *
     * The metadata for the file will be stored in the database.
     * The \Framework\Data\Storage\File can be used to retrieve the file.
     *
     * The path is the path relative to App.StorageDir and has to start with the directory separator.
     * if the path is not specified, the following path is used: App.StorageDir\App.UploadDir
     * See app.ini to view the configured directories.
     *
     * This function will return the storage name which can be used to retrieve it again.
     *
     * @param null $path
     *
     * @return string
     */
    public function store($path = null)
    {
        // Full path + name of the file
        $base_path = tempnam(
            ($path === null ? config('App.StorageDir') : $path).config('App.UploadDir'),
            ''
        );

        // The size must be read before the file is moved.
        $size = $this->getSize();

        // Just the file name
        $file_name = basename($base_path);

        // Relative storage path for flysystem
        $storage_path = config('App.UploadDir').'/'.$file_name;

        // Change tmpfilename.tmp to tmpfilename
        $storage_name = substr(basename($storage_path), 0, -4);

        $this->move(
            // Just the path (without the directory separator at the end)
            str_replace(DIRECTORY_SEPARATOR.$file_name, '', $base_path),
            $file_name
        );

        (new FileMetadata())
            ->setFileName($this->getClientOriginalName())
            ->setStoragePath($storage_path)
            ->setStorageName($storage_name)
            ->setSize($size)
            ->save();

        return new File($storage_name);
    }
}
