<?php

namespace Framework\Data\Storage\Collection;

use App\Models\FileMetadata;
use App\Models\FileMetadataQuery;
use Framework\Data\Storage\File;
use Framework\Facades\Storage;
use Framework\Util\Zip;

class FileCollection implements \ArrayAccess
{
    /**
     * All file instances.
     *
     * @var array
     */
    private $files = [];

    /**
     * FileCollection constructor.
     *
     * @param FileCollection|array $collection
     */
    public function __construct($collection = null)
    {
        if ($collection === null) {
            return;
        } elseif ($collection instanceof self) {
            $this->merge($collection);
        } elseif (is_array($collection)) {
            $this->files = $this->resolveFiles($collection);
        } else {
            throw new \InvalidArgumentException('A FileCollection can only be instantiated from another FileCollection or an array.');
        }
    }

    /**
     * Add all given files to the collection.
     *
     * @param string|File|array $file
     */
    public function add($file)
    {
        $this->files = array_merge(
            $this->files,
            $this->resolveFiles($file)
        );
    }

    /**
     * Remove all given files from the collection.
     *
     * @param string|File|array $file
     */
    public function remove($file)
    {
        $files = $this->resolveFiles($file);

        // Remove each file.
        foreach ($files as $file) {
            $key = array_keys($this->files, $file);

            if ($key !== false) {
                foreach ($key as $i) {
                    unset($this->files[$i]);
                }
            }
        }

        // Reindex the array
        $this->files = array_values($this->files);
    }

    /**
     * Move all files in the collection to the given directory.
     *
     * @param $directory
     */
    public function move(string $directory)
    {
        foreach ($this->files as $index => $file) {
            // $file->move returns a new File instance.
            $this->files[$index] = $file->move($directory);
        }
    }

    /**
     * Copy all files in the collection to the given directory.
     * This will return a new collection containing the copied files.
     *
     * @param string $directory
     *
     * @return FileCollection
     */
    public function copy(string $directory): FileCollection
    {
        $collection = new self();

        foreach ($this->files as $file) {
            // $file->copy returns a new File instance.
            $collection->add($file->copy($directory));
        }

        return $collection;
    }

    /**
     * Delete all files in the collection.
     */
    public function delete()
    {
        foreach ($this->files as $file) {
            $file->delete();
        }

        $this->files = [];
    }

    /**
     * Overwrite all content in the files in this collection.
     *
     * @param string $content
     */
    public function put(string $content)
    {
        foreach ($this->files as $file) {
            $file->put($content);
        }
    }

    /**
     * Append content to all files in the collection.
     *
     * @param string $content
     */
    public function append(string $content)
    {
        foreach ($this->files as $file) {
            $file->append($content);
        }
    }

    /**
     * Read all files in the given directory and add them to the collection.
     *
     * @param string $directory
     * @param bool   $recursive = false
     */
    public function readDirectory(string $directory, $recursive = false)
    {
        /*
         * This will return a listing of all files and directories.
         * Directories can be recognized by the absence of the 'extension' key.
         */
        $listing = Storage::listContents($directory, $recursive);

        // Array containing filenames.
        $files = [];

        foreach ($listing as $item) {
            // Check if the item is a directory.
            if (!array_key_exists('extension', $item)) {
                continue;
            }

            $files[] = $item['filename'];
        }

        /*
         * Add files in bulk, that way only one query to the database is made.
         */
        $this->add($files);
    }

    /**
     * Merges the given FileCollection into this FileCollection.
     *
     * @param FileCollection $collection
     */
    public function merge(FileCollection $collection)
    {
        $this->files = array_merge(
            $this->files,
            $collection->getFileArray()
        );
    }

    /**
     * Call a function on every file in this collection.
     *
     * @param callable $callable
     */
    public function each(callable $callable)
    {
        foreach ($this->files as $file) {
            $callable($file);
        }
    }

    /**
     * Filter the current collection.
     *
     * Possible filters:
     * size_gt (greater than in bytes)
     * size_lt (lesser than in bytes)
     * mimetype
     * extension (of the original filename)
     * executable
     *
     * @param array $filters
     *
     * @return FileCollection
     */
    public function where(array $filters): FileCollection
    {
        $files = [];

        foreach ($this->files as $file) {
            $passed = true;

            foreach ($filters as $filter => $value) {
                switch ($filter) {
                    case 'size_gt':
                        if ($file->getSize() < $value) {
                            $passed = false;
                        }
                        break;
                    case 'size_lt':
                        if ($file->getSize() > $value) {
                            $passed = false;
                        }
                        break;
                    case 'mimetype':
                        if ($file->mimetype() !== $value) {
                            $passed = false;
                        }
                        break;
                    case 'extension':
                        if ($file->extension() !== $value) {
                            $passed = false;
                        }
                        break;
                    case 'executable':
                        if ($file->isExecutable() !== $value) {
                            $passed = false;
                        }
                        break;
                }
            }

            if ($passed) {
                $files[] = $file;
            }
        }

        return new self($files);
    }

    /**
     * Compress all the files in the collection into a .zip archive.
     * The folders option will retain the original file structure.
     *
     * This will return a Zip instance.
     *
     * @param bool $folders
     *
     * @return Zip
     */
    public function zip(bool $folders = false): Zip
    {
        $zip = new Zip();

        foreach ($this->files as $file) {
            $zip->addFile(
                $file->read(),
                $folders
                    ? $file->getRelativeDirectory().'/'.$file->getOriginalName()
                    : $file->getOriginalName()
            );
        }

        return $zip;
    }

    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return sizeof($this->files) === 0;
    }

    public function getSize(): int
    {
        return sizeof($this->files);
    }

    /**
     * Return the array containing the files managed
     * by the collection.
     *
     * @return array
     */
    public function getFileArray(): array
    {
        return $this->files;
    }

    /**
     * Helper function that can get one or more file instances
     * depending on what is passed to it.
     *
     * @param string|File|array $file
     *
     * @return array
     */
    private function resolveFiles($file)
    {
        if ($file instanceof File) {
            return [$file];
        }

        if ($file instanceof FileMetadata) {
            return [new File($file)];
        }

        if (is_array($file)) {
            return $this->resolveBulk($file);
        }

        if (is_string($file)) {
            return [new File($file)];
        }
    }

    /**
     * Resolves a list of filenames or File instances to an array of File instances.
     * This will only execute one query to the database.
     *
     * @param array $list
     *
     * @return array
     *
     * @throws \Exception
     */
    private function resolveBulk(array $list): array
    {
        if ($list === []) {
            return [];
        }

        $file_names = [];
        $files = [];

        foreach ($list as $file) {
            if ($file instanceof File) {
                $files[] = $file;
            } elseif ($file instanceof FileMetadata) {
                $files[] = new File($file);
            } elseif (is_string($file)) {
                $file_names[] = $file;
            } else {
                throw new \Exception('Only File instances and strings are allowed in bulk resolving.');
            }
        }

        $files_found = FileMetadataQuery::create()->findByStorageName($file_names);

        foreach ($files_found as $file) {
            $files[] = new File($file);
        }

        return $files;
    }

    /**
     * Whether a offset exists.
     *
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     *
     * @param mixed $offset <p>
     *                      An offset to check for.
     *                      </p>
     *
     * @return bool true on success or false on failure.
     *              </p>
     *              <p>
     *              The return value will be casted to boolean if non-boolean was returned
     *
     * @since 5.0.0
     */
    public function offsetExists($offset)
    {
        return isset($this->files[$offset]);
    }

    /**
     * Offset to retrieve.
     *
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     *
     * @param mixed $offset <p>
     *                      The offset to retrieve.
     *                      </p>
     *
     * @return mixed Can return all value types
     *
     * @since 5.0.0
     */
    public function offsetGet($offset)
    {
        return isset($this->files[$offset]) ? $this->files[$offset] : null;
    }

    /**
     * Offset to set.
     *
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     *
     * @param mixed $offset <p>
     *                      The offset to assign the value to.
     *                      </p>
     * @param mixed $value  <p>
     *                      The value to set.
     *                      </p>
     *
     * @since 5.0.0
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->files[] = $value;
        } else {
            $this->files[$offset] = $value;
        }
    }

    /**
     * Offset to unset.
     *
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     *
     * @param mixed $offset <p>
     *                      The offset to unset.
     *                      </p>
     *
     * @since 5.0.0
     * @return mixed|null|void
     */
    public function offsetUnset($offset)
    {
        return isset($this->files[$offset]) ? $this->files[$offset] : null;
    }
}
