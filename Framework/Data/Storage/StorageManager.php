<?php

namespace Framework\Data\Storage;

use Framework\Config;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;

class StorageManager
{
    /**
     * @var Local
     */
    protected $adapter;

    /**
     * @var Filesystem
     */
    protected $filesystem;

    public function __construct()
    {
        $this->adapter = new Local(config('App.StorageDir'));
        $this->filesystem = new Filesystem($this->adapter);
    }

    /**
     * Call a Filesystem function.
     *
     * @param $name
     * @param $arguments
     *
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        return call_user_func_array([$this->filesystem, $name],  $arguments);
    }
}
