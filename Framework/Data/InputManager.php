<?php

namespace Framework\Data;

use Framework\Data\Storage\UploadedFile;

class InputManager extends ArrayMutater
{
    /**
     * In the constructor the method is determined.
     * InputManager constructor.
     */
    public function __construct()
    {
        $i = request()->method();

        if ($i == 'GET') {
            $this->array = $_GET;
        } elseif ($i == 'POST') {
            $this->array = $_POST;
        } else {
            parse_str(file_get_contents('php://input'), $this->array);
        }
    }

    /**
     * Get a value and return it sanitized.
     *
     * @param string $key
     * @param string $allowedTags
     *
     * @return string
     */
    public function sanitize(string $key, string $allowedTags = '')
    {
        return strip_tags($this->get($key), $allowedTags);
    }

    /**
     * Get multiple values and return them sanitized.
     *
     * @param array  $keys
     * @param string $allowedTags
     *
     * @return array
     */
    public function sanitizeMultiple(array $keys, string $allowedTags = '')
    {
        foreach ($keys as $key) {
            $result[$key] = $this->sanitize($key, $allowedTags);
        }

        return $result;
    }

    /**
     * Get multiple items.
     *
     * @param array $keys
     * @param null  $default
     *
     * @return array
     */
    public function getMultiple(array $keys, $default = null)
    {
        foreach ($keys as $key) {
            $result[$key] = $this->get($key, $default);
        }

        return $result;
    }

    /**
     * Merge the current input array with the given array.
     *
     * @param array $array
     */
    public function merge(array $array)
    {
        $this->array = array_merge($this->array, $array);
    }

    /**
     * Retrieve a file.
     * If multiple files are uploaded using the same input (using 'multiple' on the file input),
     * a index should be specified to retrieve te right file.
     * If multiple files are uploaded under the same name and no index is given, then an array of UploadedFile's is returned.
     *
     * @param $key
     * @param null $index
     *
     * @return bool|UploadedFile
     */
    public function file($key, $index = null)
    {
        // Single file in a multiple file upload
        if ($index !== null) {
            if (!$this->fileExists($key, $index)) {
                return false;
            }

            return new UploadedFile(
                $_FILES[$key]['tmp_name'][$index],
                $_FILES[$key]['name'][$index]
            );
        }

        // All files in a multiple file upload
        if (is_array($_FILES[$key]['tmp_name'])) {
            $files = [];

            for ($i = 0; $i < sizeof($_FILES[$key]['tmp_name']); ++$i) {
                if (!$this->fileExists($key, $i)) {
                    return false;
                }

                $files[] = new UploadedFile(
                    $_FILES[$key]['tmp_name'][$i],
                    $_FILES[$key]['name'][$i]
                );
            }

            return $files;
        }

        // Single file
        if (!$this->fileExists($key)) {
            return false;
        }

        return new UploadedFile(
            $_FILES[$key]['tmp_name'],
            $_FILES[$key]['name']
        );
    }

    /**
     * Retrieve whether a file exists.
     * When multiple files are uploaded using one input, an index can be given
     * to check the file at that specific index for errors.
     *
     * @param $key
     * @param null $index
     *
     * @return bool
     */
    public function fileExists($key, $index = null): bool
    {
        if (!array_key_exists($key, $_FILES)) {
            return false;
        }

        // Singular file upload
        if ($index === null) {
            return $_FILES[$key]['error'] !== UPLOAD_ERR_NO_FILE;
        }

        // Multiple file upload
        return $_FILES[$key]['error'][$index] !== UPLOAD_ERR_NO_FILE;
    }
}