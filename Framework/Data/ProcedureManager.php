<?php

namespace Framework\Data;

/**
 * The ProcedureManager serves as a proxy between the Procedure facade
 * and the procedures in the App/Procedures directory. It instantiates and executes them.
 * Class ProcedureManager.
 */
class ProcedureManager
{
    public function __call($name, $arguments)
    {
        $className = '\\App\\Procedure\\'.ucwords($name);
        $instance = new $className();

        return $instance->run(...$arguments);
    }
}
