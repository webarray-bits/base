<?php

namespace Framework\Data\Mail;

use Framework\Config;
use Framework\Facades\Storage;
use PHPMailer;

/*
 * TODO: allow for a queueing system to offload mails
 * TODO: implement MailGun or other third party mail drivers
 *
 * TODO:: create a good MailManager
 */
class MailManager
{
    /**
     * @var PHPMailer
     */
    private $mailer;

    /**
     * @var array
     */
    private $buffer;

    /**
     * @var array
     */
    private $mailBuffer;

    /**
     * @var mixed
     */
    private $config;

    /**
     * MailManager constructor.
     */
    public function __construct()
    {
        // Load configuration and instantiate PHPMailer
        $this->config = config('Mail.Configuration');

        $this->mailer = new PHPMailer();

        // UTF-8
        $this->mailer->CharSet = $this->config['Encoding'];

        // Run setup
        $this->setup();
    }

    /**
     * Set up the mail configuration.
     *
     * @throws \Whoops\Exception\ErrorException
     */
    private function setup()
    {
        // WordWrap is used in all mail configurations
        $this->mailer->WordWrap = (int) trim($this->config['WordWrap']);

        // Setup SMTP or MailGun configuration. The toDisk method does not require
        // additional configuration.
        switch (config('Mail.Driver')) {
            case 'SMTP':
                $this->mailer->isSMTP();
                $this->mailer->SMTPDebug = $this->config['SMTPDebug'];
                $this->mailer->SMTPAuth = $this->config['SMTPAuth'];
                $this->mailer->SMTPSecure = $this->config['SMTPSecure'];
                $this->mailer->Host = $this->config['Host'];
                $this->mailer->Username = $this->config['Username'];
                $this->mailer->Password = $this->config['Password'];
                $this->mailer->Sender = $this->config['Username'];
                $this->mailer->Port = $this->config['Port'];
                break;
            case 'Mailgun':
                throw new \Exception('Mailgun driver not yet implemented!');
                break;
        }
    }

    /**
     * Flush mail buffer on destruction.
     *
     * @throws \Whoops\Exception\ErrorException
     */
    public function __destruct()
    {
        // Mails are send on class destruction
        // ToDisk will simply append the buffer into the mail.log file
        // If this file does not exist, it is created
        if (config('Mail.Driver') === 'ToDisk') {
            if (!Storage::has('mail.log')) {
                Storage::write('mail.log', '');
            }

            $log = Storage::read('mail.log');
            $log .= $this->buffer;

            // Convert <br> to linebreaks for the mail.log
            Storage::put('mail.log', preg_replace('#<br\s*/?>#i', "\n", $log));

            return;
        }

        // Otherwise send every mail in the mailbuffer
        // This allows to send differnt types of email to different recipients in one request
        try {
            foreach ($this->mailBuffer as $mail) {
                $this->mailer->isHTML($mail['isHTML']);

                foreach ($mail['to'] as $recipient) {
                    $this->mailer->addAddress($recipient['address'], $recipient['name']);
                }

                foreach ($mail['cc'] as $recipient) {
                    $this->mailer->addCC($recipient['address'], $recipient['name']);
                }

                foreach ($mail['bcc'] as $recipient) {
                    $this->mailer->addBCC($recipient['address'], $recipient['name']);
                }

                foreach ($mail['from'] as $recipient) {
                    $this->mailer->setFrom($recipient['address'], $recipient['name']);
                }

                $this->mailer->Subject = $mail['subject'];
                $this->mailer->Body = $mail['message'];
                $this->mailer->send();
            }
        } catch (\Exception $e) {
            throw new \Exception('Failed to send email: '.$this->mailer->ErrorInfo);
        }
    }

    // TODO: implement this
    private function mailgun()
    {
    }

    public function to($users)
    {
        return (new MailableMailer($this))->to($users);
    }

    /**
     * @param array      $to
     * @param array|null $cc
     * @param array|null $bcc
     * @param array|null $from
     * @param string     $subject
     * @param string     $message
     * @param bool       $isHTML
     */
    public function send(array $to, array $cc, array $bcc, array $from, string $subject, string $message, bool $isHTML = false)
    {
        // SMTP and ToDisk will simply append to their appropriate buffers
        // TODO: implement MailGun
        switch (config('Mail.Driver')) {
            case 'SMTP':
                $this->mailBuffer[] = compact('to', 'cc', 'bcc', 'from', 'subject', 'message', 'isHTML');
                break;
            case 'Mailgun':
                break;
            case 'ToDisk':
                // $this->buffer .= "\nFrom " . $from[0]['address'] . ', ' . $from[0]['name'] . "\nTo " . $to[0]['address'] . ', ' . $to[0]['name'] . "\nSubject: $subject\n\n$message\n------------------------------------------------------------------------------\n";
                $this->buffer .= view('Email.ToDisk', compact('to', 'cc', 'bcc', 'from', 'subject', 'message'));
                break;
        }
    }

    /**
     * Send an email to multiple recipients.
     *
     * @param array  $to
     * @param string $subject
     * @param string $message
     * @param bool   $isHTML
     *
     * @deprecated Because the to function can understand arrays and user objects
     */
    public function sendToMultiple(array $to, string $subject, string $message, bool $isHTML = false)
    {
        foreach ($to as $recipient) {
            $this->send($recipient, $subject, $message, $isHTML);
        }
    }
}
