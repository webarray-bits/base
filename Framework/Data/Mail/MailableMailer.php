<?php

namespace Framework\Data\Mail;

use App\Models\User;
use Framework\Config;
use Framework\Facades\View;
use Propel\Runtime\Collection\ObjectCollection;

class MailableMailer
{
    private $to = [];
    private $cc = [];
    private $bcc = [];
    private $from = [];
    private $subject;
    private $text;
    private $view;
    private $data = [];
    private $isHTML = false;
    private $mailer;

    /**
     * MailableMailer constructor.
     *
     * @param MailManager $mailer
     */
    public function __construct(MailManager $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param $address
     * @param null $name
     *
     * @return MailableMailer
     */
    public function to($address, $name = null)
    {
        return $this->setAddress($address, $name, 'to');
    }

    /**
     * @param $address
     * @param null $name
     *
     * @return MailableMailer
     */
    public function cc($address, $name = null)
    {
        return $this->setAddress($address, $name, 'cc');
    }

    /**
     * @param $address
     * @param null $name
     *
     * @return MailableMailer
     */
    public function bcc($address, $name = null)
    {
        return $this->setAddress($address, $name, 'bcc');
    }

    /**
     * @param $address
     * @param null $name
     *
     * @return MailableMailer
     */
    public function from($address, $name = null)
    {
        return $this->setAddress($address, $name, 'from');
    }

    /**
     * @param string $subject
     *
     * @return $this
     *
     * @TODO:: Create prefix for subject in config.
     */
    public function subject(string $subject)
    {
        $this->subject = $subject;
        $this->setData(compact('subject'));

        return $this;
    }

    public function text($text)
    {
        $this->text = $text;

        return $this;
    }

    private function setData(array $data)
    {
        $this->data = array_merge($this->data, $data);
    }

    public function data(array $data)
    {
        $this->setData($data);

        return $this;
    }

    public function view(string $view, array $data = [])
    {
        $this->view = $view;
        $this->setData($data);
        $this->isHTML = true;

        return $this;
    }

    public function send()
    {
        $this->mailer->send(
            $this->to,
            $this->cc,
            $this->bcc,
            $this->createFrom(),
            $this->subject,
            $this->createBody(),
            $this->isHTML
        );
    }

    private function createBody()
    {
        if (isset($this->view)) {
            return View::render($this->view, $this->data);
        } elseif (isset($this->text)) {
            return $this->text;
        } else {
            throw new \Exception('A mail Without body is no mail');
        }
    }

    private function createFrom()
    {
        if (empty($this->from)) {
            $this->from(
                config('Mail.Configuration.From'),
                config('Mail.Configuration.FromName')
            );
        }

        return $this->from;
    }

    /**
     * Set the recipients of the message.
     *
     * @param object|array|string $address
     * @param string|null         $name
     * @param string              $property
     *
     * @return $this
     *
     * @throws \Exception
     */
    private function setAddress($address, $name, string $property)
    {
        if ($address instanceof User) {
            $this->{$property}($address->getEmail(), $address->getFullName());
        }

        // We can not know if a ObjectCollection is a user. So this will do
        elseif ($address instanceof ObjectCollection) {
            foreach ($address as $user) {
                $this->{$property}($user);
            }
        } elseif (is_array($address)) {
            if (isset($address['email'])) {
                $this->{$property}($address['email'], $address['name']);
            } else {
                foreach ($address as $user) {
                    $this->{$property}($user['email'], $user['name']);
                }
            }
        } elseif (is_string($address)) {
            $this->{$property}[] = compact('address', 'name');
        } else {
            throw new \Exception("'$address' is not a valid Address.");
        }

        return $this;
    }
}
