<?php

namespace Framework\Data;

/*
    This interface is to assure all classes which perform standard mutations to arrays
    to adhere to the standard mutation naming scheme which is used throughout the framework.
*/
interface ArrayMutable
{
    public function get(string $key, $default = null);
    public function set(string $key, $value);
    public function forget(string $key);
    public function exists(string $key):bool;
}
