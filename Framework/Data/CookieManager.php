<?php

namespace Framework\Data;

class CookieManager
{
    /**
     * Get a cookie.
     *
     * @param string $key
     *
     * @return mixed|null
     */
    public function get(string $key)
    {
        if ($this->exists($key)) {
            return json_decode($_COOKIE[ config('App.Key').$key ]);
        }

        return null;
    }

    /**
     * Set a cookie.
     *
     * @param string $key
     * @param $value
     * @param int $seconds
     */
    public function set(string $key, $value, $seconds = 3600)
    {
        setcookie(
            config('App.Key').$key, // name
            json_encode($value), // value
            (int) (time() + $seconds), // expire
            config('Cookie.path'), // path
            config('Cookie.domain'), // domain
            config('Cookie.secure'), // secure
            config('Cookie.httponly') // httponly
        );
    }

    /**
     * Set a cookie for 5 years.
     *
     * @param string $key
     * @param $value
     */
    public function forever(string $key, $value)
    {
        $this->set($key, $value, years(5));
    }

    /**
     * Remove a cookie.
     *
     * @param string $key
     */
    public function forget(string $key)
    {
        // Unset cookie by specifying a time in the past
        if ($this->exists($key)) {
            // Will reset cookie(client,browser)
            $this->set($key, null, -years(5));
            // Will destroy cookie(server)
            unset($_COOKIE[config('App.Key').$key]);
        }
    }

    /**
     * Does a cookie exist?
     *
     * @param string $key
     *
     * @return bool
     */
    public function exists(string $key): bool
    {
        return isset($_COOKIE[config('App.Key').$key]);
    }
}
