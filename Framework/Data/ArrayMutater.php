<?php
/**
 * Created by PhpStorm.
 * User: Lex
 * Date: 30-7-2016
 * Time: 20:03.
 */

namespace Framework\Data;

use Framework\Util\DotArray;

class ArrayMutater implements ArrayMutable
{
    /**
     * The array containing the data.
     *
     * @var array
     */
    protected $array;

    public function get(string $key, $default = null)
    {
        return DotArray::get($this->array, $key, $default);
    }

    public function getArray()
    {
        return $this->array;
    }

    public function pull(string $key, $default = null)
    {
        return DotArray::pull($this->array, $key, $default);
    }

    public function set(string $key, $value)
    {
        DotArray::set($this->array, $key, $value);
    }

    public function add(string $key, $value)
    {
        DotArray::add($this->array, $key, $value);
    }

    public function forget(string $key)
    {
        DotArray::forget($this->array, $key);
    }

    public function exists(string $key):bool
    {
        return DotArray::exists($this->array, $key);
    }
}
