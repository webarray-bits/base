<?php

namespace Framework\Data;

use SuperClosure\Analyzer\TokenAnalyzer;
use SuperClosure\Serializer;

/**
 * Class SerializationManager.
 */
class SerializationManager
{
    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * SerializationManager constructor.
     *
     * @internal param $serializer
     */
    public function __construct()
    {
        $this->serializer = new Serializer(new TokenAnalyzer());
    }

    /**
     * Serialize a variable, object or closure.
     *
     * @param $data
     *
     * @return string
     */
    public function do($data): string
    {
        if (is_callable($data)) {
            return $this->serializer->serialize($data);
        }

        return serialize($data);
    }

    /**
     * Unserialize a variable, object or closure.
     *
     * @param string $data
     * @param bool   $isClosure
     *
     * @return mixed
     */
    public function undo($data, bool $isClosure = false)
    {
        if (!$isClosure) {
            return unserialize($data);
        }

        return $this->serializer->unserialize($data);
    }
}
