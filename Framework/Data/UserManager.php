<?php

namespace Framework\Data;

use App\Data\Roles;
use App\Models\RememberMeQuery;
use App\Models\User;
use Framework\Config;
use Framework\Facades\Cookie;
use Framework\Facades\Procedure;
use Framework\Facades\Session;

class UserManager
{
    /**
     * The user object, as retrieved from the database.
     *
     * @var \App\Models\User
     */
    private $user;

    public function __construct()
    {
        if ($this->loggedIn()) {
            $this->user = Session::get('user');
        }
    }

    /**
     * Return whether a user is logged in.
     *
     * @return bool
     */
    public function loggedIn():bool
    {
        if (!Session::exists('user') && Cookie::exists(config('App.RememberMeCookie'))) {
            Procedure::loginWithCookie(Cookie::get(config('App.RememberMeCookie')));
        }

        return Session::exists('user');
    }

    /**
     * If the user is logged in, return the user object. Otherwise returns null.
     *
     * @return User|null
     */
    public function get()
    {
        if ($this->loggedIn()) {
            return $this->user;
        }

        return null;
    }

    /**
     * if this user is administrator or has the specified role, return true. Otherwise return false.
     *
     * @param string $role
     *
     * @return bool
     */
    public function hasRole(string $role): bool
    {
        return (bool) array_intersect(
            [$role, Roles::Administrator],
            $this->get()->getRoles()->toKeyValue('id', 'workName')
        );
    }

    /**
     * Log the user out.
     */
    public function logout()
    {
        Session::forget('user');

        if (Cookie::exists(config('App.RememberMeCookie'))) {
            list($user_id, $token, $hash) = explode(':', Cookie::get(config('App.RememberMeCookie')));

            if (!empty($token) && $hash === hash('sha256', $user_id.':'.$token)) {
                // Delete Token from db
                RememberMeQuery::create()
                    ->filterByUser($this->user)
                    ->filterByToken($token)
                    ->delete();
            }

            // Delete the remember me cookie so the user will not get logged in immediately
            Cookie::forget(config('App.RememberMeCookie'));
        }
    }
}
