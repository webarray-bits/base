<?php

namespace Framework;

use Carbon\Carbon;
use Framework\Event\Kernel\KernelStartEvent;
use Framework\Event\Kernel\KernelResponseEvent;
use Framework\Event\Kernel\KernelStopEvent;
use Framework\Http\Response;
use Framework\Http\Router;
use Framework\Service\ServiceRegistrar;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Whoops\Handler\JsonResponseHandler;
use Whoops\Handler\PrettyPageHandler;
use Whoops\Run;

class Kernel
{
    /**
     * The ServiceRegistrar.
     *
     * @var ServiceRegistrar
     */
    protected $registrar;

    /**
     * The basepath of the application.
     *
     * @var string
     */
    protected $basepath;

    /**
     * Boot the kernel.
     */
    public function start()
    {
        $this->setup();
        $this->services();

        if (!app()->inConsoleMode()) {
            $this->route();
        } else {
            /*
             * The kernel.stop event fires when the kernel stops without returning a response.
             * This is the case when the application is in console mode.
             */
            $this->registrar->dispatch('kernel.stop', new KernelStopEvent());
        }
    }

    protected function setup()
    {
        /*
         * Set up Monolog logging.
         */
        $file = config('App.Logging.File');
        $logger = Container::set(Logger::class, new Logger('File', [
            new StreamHandler($file),
        ]));

        /*
         * If enabled, set up Whoops for pretty errors.
         */
        if (config('App.ShowWhoops', false) && !app()->inConsoleMode()) {
            (new Run())
                ->pushHandler(request()->isAjax() ? new JsonResponseHandler() : new PrettyPageHandler())
                ->pushHandler(function ($exception, $inspector, $run) use ($logger) {
                    $logger->addError((string) $exception);
                })
                ->register();
        }

        // Configure the default timezone
        date_default_timezone_set(config('App.Timezone'));

        // Set the correct locale
        setlocale(LC_TIME, config('App.Locale'));

        // Set the locale for Carbon explicitly
        Carbon::setLocale(config('App.Locale'));
    }

    protected function services()
    {
        $this->registrar = Container::get(ServiceRegistrar::class);
        $this->registrar->initialize();
        $this->registrar->dispatch('kernel.start', new KernelStartEvent());
    }

    protected function route()
    {
        /**
         * @var Response
         */
        $response = (new Router())->handleRequest();

        /*
         * The kernel.response event is dispatched so that
         * services can modify the response object before it is returned.
         */
        $this->registrar->dispatch(
            'kernel.response',
            new KernelResponseEvent($response)
        );

        $response->send();
    }
}
