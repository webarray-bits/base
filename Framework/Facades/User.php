<?php

namespace Framework\Facades;

use Framework\Data\UserManager;
use Framework\Facades\Base\Facade;

/**
 * @method static bool loggedIn()
 * @method static array|bool role()
 * @method static string|bool username()
 * @method static void logout()
 * @method static bool hasRole(string $role)
 * @method static bool|int id()
 * @method static \App\Models\User|\App\Models\Profile|null get()
 * Class User
 */
class User extends Facade
{
    public static function resolve()
    {
        return UserManager::class;
    }
}
