<?php

namespace Framework\Facades;

use Framework\Data\ImageManager;
use Framework\Facades\Base\Facade;

/**
 * @method static \Intervention\Image\Image make($data)
 * @method static \Intervention\Image\Image canvas($width, $height, $background = null)
 * @method static Image cache(\Closure $callback, $lifetime = null, $returnObj = false)
 * @method static void checkRequirements()
 * Class Image
 */
class Image extends Facade
{
    public static function resolve()
    {
        return ImageManager::class;
    }
}
