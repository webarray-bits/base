<?php

namespace Framework\Facades;

use Framework\Data\SessionManager;
use Framework\Facades\Base\Facade;

/**
 * @method static mixed get($key, $default = null)
 * @method static array getAll()
 * @method static void set($key, $value)
 * @method static void append($array, $value)
 * @method static void appendKey($array, $key, $value)
 * @method static void deleteKey($array, $key)
 * @method static void forget($key)
 * @method static bool exists($key)
 * Class Session
 */
class Session extends Facade
{
    public static function resolve()
    {
        return SessionManager::class;
    }
}
