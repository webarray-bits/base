<?php

namespace Framework\Facades\Base;

use Framework\Container;

class Facade
{
    public static function __callStatic($name, $arguments)
    {
        return call_user_func_array([Container::get(static::resolve()), $name], $arguments);
    }
}
