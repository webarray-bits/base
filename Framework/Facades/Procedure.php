<?php

namespace Framework\Facades;

use Framework\Data\ProcedureManager;
use Framework\Facades\Base\Facade;

/**
 * @method static bool login(string $request)
 * @method static void loginWithCookie($cookie)
 * @method static bool register(string $request)
 * Class Procedure
 */
class Procedure extends Facade
{
    public static function resolve()
    {
        return ProcedureManager::class;
    }
}
