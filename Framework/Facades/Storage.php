<?php

namespace Framework\Facades;

use Framework\Data\Storage\StorageManager;
use Framework\Facades\Base\Facade;

/**
 * @method static void write(string $path, string $contents)
 * @method static void update(string $path, string $contents)
 * @method static void put(string $path, string $contents)
 * @method static string read(string $path)
 * @method static bool has(string $path)
 * @method static void delete(string $path)
 * @method static string readAndDelete(string $path)
 * @method static void rename(string $source, string $target)
 * @method static void copy(string $source, string $destination)
 * @method static void getMimeType(string $path)
 * @method static mixed getTimeStamp(string $path)
 * @method static mixed getSize(string $path)
 * @method static void createDir(string $path)
 * @method static void deleteDir(string $path)
 * @method static bool exists(string $path)
 * @method static array listContents(string $directory = null, bool $recursive = false)
 * @method static bool writeStream($path, $resource, array $config = [])
 * @method static bool updateStream($path, $resource, array $config = [])
 * @method static stream readStream($path)
 * @method static bool putStream($path, $resource, array $config = [])
 *
 * Class File
 */
class Storage extends Facade
{
    public static function resolve()
    {
        return StorageManager::class;
    }
}
