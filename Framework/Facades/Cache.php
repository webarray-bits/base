<?php

namespace Framework\Facades;

use Framework\Data\CacheManager;
use Framework\Facades\Base\Facade;

/**
 * @method static void set(string $key, $value, int $Seconds =600)
 * @method static void forever(string $key, $value)
 * @method static bool has(string $key)
 * @method static mixed get(string $key, $default = null)
 * @method static mixed pull(string $key)
 * @method static void forget(string $key)
 * @method static mixed remember(string $key, int $minutes, \Closure $function)
 * @method static mixed rememberForever(string $key, \Closure $function)
 * @method static void clean()
 * @method static array stats()
 * Class Cache
 */
class Cache extends Facade
{
    public static function resolve()
    {
        return CacheManager::class;
    }
}
