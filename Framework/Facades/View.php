<?php

namespace Framework\Facades;

use Framework\Views\ViewManager;
use Framework\Facades\Base\Facade;

/**
 * @method static void addData(array $data)
 * @method static string render(string $name, array $vars = [])
 * @method static string error($name)
 * @method static string customError(string $title, string $content)
 * Class View
 */
class View extends Facade
{
    protected static function resolve()
    {
        return ViewManager::class;
    }
}
