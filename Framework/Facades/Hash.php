<?php

namespace Framework\Facades;

use Framework\Util\HashManager;
use Framework\Facades\Base\Facade;

/**
 * @method static string make(string $toHash)
 * @method static bool match(string $toMatch, string $hash)
 * @method static bool needsRehash(string $hash)
 * Class Hash
 */
class Hash extends Facade
{
    public static function resolve()
    {
        return HashManager::class;
    }
}
