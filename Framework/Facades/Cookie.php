<?php

namespace Framework\Facades;

use Framework\Data\CookieManager;
use Framework\Facades\Base\Facade;

/**
 * @method static mixed get(string $key)
 * @method static void set(string $key, $value, int $seconds = 3600)
 * @method static void forever(string $key, $value)
 * @method static void forget(string $key)
 * @method static bool exists(string $key)
 * Class Session
 */
class Cookie extends Facade
{
    public static function resolve()
    {
        return CookieManager::class;
    }
}
