<?php

namespace Framework\Facades;

use Framework\Data\InputManager;
use Framework\Facades\Base\Facade;

/**
 * @method static mixed sanitize(string $key, $allowedTags = '')
 * @method static mixed sanitizeMultiple(array $keys, $allowedTags = '')
 * @method static array getArray()
 * @method static mixed get(string $key, $default = null)
 * @method static mixed getMultiple(array $keys, $default = null)
 * @method static void set(string $key, $value)
 * @method static void merge(array $array)
 * @method static void forget(string $key)
 * @method static bool exists(string $key)
 * @method static mixed file(string $key, $index = null)
 * @method static bool fileExists(string $key)
 * Class Input
 */
class Input extends Facade
{
    public static function resolve()
    {
        return InputManager::class;
    }
}
