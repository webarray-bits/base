<?php

namespace Framework\Facades;

use Framework\Data\Mail\MailableMailer;
use Framework\Data\Mail\MailManager;
use Framework\Facades\Base\Facade;

/**
 * @method static MailableMailer to($users, $name = null)
 * @method static void send(string $to, string $subject, string $message, bool $isHTML = false)
 * @method static void sendToMultiple(array $to, string $subject, string $message, bool $isHTML = false)
 * Class Mail
 */
class Mail extends Facade
{
    public static function resolve()
    {
        return MailManager::class;
    }
}
