<?php

namespace Framework\Facades;

use Framework\Data\SerializationManager;
use Framework\Facades\Base\Facade;

/**
 * @method static string do($data)
 * @method static mixed undo($data, bool $isClosure = false)
 * Class Serialize
 */
class Serialize extends Facade
{
    public static function resolve()
    {
        return SerializationManager::class;
    }
}
