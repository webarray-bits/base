<?php

namespace Framework\Http;

use Framework\Container;
use Framework\Data\Storage\File;
use Framework\Facades\Cookie;
use Framework\Facades\Storage;
use Framework\Facades\Input;
use Framework\Facades\Session;
use Framework\Facades\View;
use Framework\Traits\Expandable;
use Framework\Traits\IExpandable;
use Framework\Util\Zip;
use Symfony\Component\HttpFoundation\Response as BaseResponse;
use function Stringy\create as s;

/*
 * TODO (lex): In de applicatieconfiguratie http-caching opties configureerbaar maken.
 */
class Response extends BaseResponse implements IExpandable
{
    use Expandable;

    /**
     * All registered extension methods.
     *
     * @var array
     */
    protected $extensions = [];

    /**
     * @param string $name
     * @param $extension
     */
    public function registerExtension(string $name, $extension)
    {
        $this->extensions[$name] = $extension;
    }

    /**
     * @param string $name
     */
    public function unregisterExtension(string $name)
    {
        unset($this->extensions[$name]);
    }

    /**
     * Encode a object as JSON, and sets the response content.
     *
     * @param $object
     *
     * @return Response
     */
    public function json($object): Response
    {
        if ($this->content === null || $this->content === '') {
            $this->content = json_encode($object);
        } else {
            $this->setContent(json_encode(array_merge(json_decode($this->content, true), $object)));
        }

        return $this;
    }

    /**
     * Renders a error view, and sets it as the response content.
     *
     * @param $name
     * @param int $code
     *
     * @return Response
     */
    public function error($name, $code = 500): Response
    {
        $this->setContent(
            View::error($name)
        );

        $this->setStatusCode($code);

        return $this;
    }

    /**
     * Redirect to the previous page.
     */
    public function back()
    {
        if (!isset($_SESSION['previous_url'])) {
            header('Location: /');
        }

        header('Location: '.$_SESSION['previous_url']);
        die();
    }

    /**
     * Redirect to the given url or action.
     *
     * @param string $target
     *
     * @return $this|null
     */
    public function redirect(string $target)
    {
        $s = s($target);

        // Check if the $target is an action.
        if ($s->contains('@') && !$s->contains('/')) {
            $target = Container::get(RouteRegistry::class)->getActionUrl($target);
        }

        if (request()->isAjax()) {
            $this->json(['redirect' => $target]);

            return $this;
        }

        header('Location: '.$target);
        die();
    }

    /**
     * Set a cookie.
     *
     * @param string $key
     * @param $value
     * @param int $seconds
     *
     * @return Response
     */
    public function cookie(string $key, $value, $seconds = 3600): Response
    {
        Cookie::set($key, $value, $seconds);

        return $this;
    }

    public function downloadFromString($filename, $content)
    {
        header('Content-Disposition: attachment; filename="'.$filename.'"');
        header('Content-Type: text/plain');
        header('Content-Length: '.strlen($content));
        header('Pragma: no-cache');
        header('Expires: 0');
        header('Connection: close');

        die($content);
    }

    /**
     * Initiate a file download.
     * If a Zip file is given, the filename is required.
     *
     * @param string|File|Zip $file
     * @param string $filename
     * @throws \Exception
     */
    public function download($file, string $filename = '')
    {
        if ($file instanceof Zip) {
            if ($filename === '') {
                throw new \InvalidArgumentException('Filename must be given when downloading a zip archive.');
            }

            $name = $filename;
            $mimetype = 'application/zip';
            $content = $file->file();
        } else {
            /*
            * If it is a string, the relative path has been given.
            * Else, a File instance has been given.
            */
            $file_path = is_string($file) ? $file : $file->getRelativePath();

            if (!Storage::has($file_path)) {
                throw new \Exception("File $file_path does not exist.");
            }

            /*
             * Resolve the name.
             */
            if ($filename !== '') {
                $name = $filename;
            } elseif (is_string($file)) {
                $name = basename($file_path);
            } else {
                $name = $file->getOriginalName();
            }

            $mimetype = Storage::getMimeType($file_path);
            $content = Storage::read($file_path);
        }

        header('Content-disposition: attachment; filename="'.$name.'"');
        header('Content-Type: '.$mimetype);
        header('Content-Transfer-Encoding: Binary');
        header('Pragma: no-cache');
        header('Expires: 0');

        echo $content;
        die();
    }

    /**
     * @return bool
     */
    public function isContentSet(): bool
    {
        return $this->getContent() !== '';
    }

    /**
     * Pushes the current Request input to the session for use with the old() Twig function.
     */
    public function withInput(): Response
    {
        Session::set('last_input', Input::getArray());

        return $this;
    }

    public function setHeaders($key, $values, $replace = true)
    {
        $this->headers->set($key, $values, $replace);

        return $this;
    }
}
