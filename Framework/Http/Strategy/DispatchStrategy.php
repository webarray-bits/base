<?php

namespace Framework\Http\Strategy;

use Framework\Http\Response;

interface DispatchStrategy
{
    public function fire(array $info): Response;
}
