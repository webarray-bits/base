<?php

namespace Framework\Http\Strategy;

use Framework\Config;
use Framework\Container;
use Framework\Controller\Controller;
use Framework\Http\Response;

class WebDispatchStrategy implements DispatchStrategy
{
    public function fire(array $info): Response
    {
        /*
        * Actions are defined with the Controller@Function syntax.
        * Using explode will result in the following array:
        * [0]  => Controller name
        * [1]  => Function name
        */
        $action = explode('@', $info[1]);
        $production = config('App.Production');

        // Extract class and function from $action
        $class = 'App\\Controllers\\'.$action[0];
        $function = $action[1];

        // Set the request variables
        request()->variables($info[2]);

        /*
         * Reflection code does not have to execute in production.
         */
        if (!$production) {
            /*
             * If the class, or the function in the class,
             * is not found we return an error.
             */
            if (!class_exists($class)) {
                throw new \Exception("$class does not exist. Is it in the right namespace?");
            }
        }

        /*
         * We retrieve an instance of the controller using the
         * dependency injector. This allows for the constructor to be
         * dependency injected in the constructor.
         */
        $controller = Container::get($class);

        if (!$production) {
            /*
             * The controller has to inherit from the base Controller class.
             */
            if (!is_subclass_of($class, Controller::class)) {
                throw new \Exception("Class '$class' does not inherit from Controller");
            }

            /*
             * The method specified in the Endpoint should exists.
             */
            if (!method_exists($controller, $function)) {
                throw new \Exception("Method '$function' does not exists in $class.");
            }

            /*
             * We use reflection to look at the accessibility of the
             * specified method. It should be public.
             */
            $reflection = new \ReflectionMethod($class, $function);

            if (!$reflection->isPublic()) {
                throw new \Exception("Method '$function' is not public.");
            }
        }

        /*
         * We execute all middleware registered on the controller.
         */
        if (($m = $controller->runMiddleware()) !== true) {
            return $m;
        }

        /*
         * Call the specified method with the arguments.
         * This is dependency injected.
         */
        $result = Container::call([$controller, $function], $info[2]);

        if (!($result instanceof Response)) {
            throw new \Exception('Controller must return Response object');
        }

        return $result;
    }
}
