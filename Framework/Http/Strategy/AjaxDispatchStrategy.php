<?php

namespace Framework\Http\Strategy;

use Framework\Container;
use Framework\Http\Response;

class AjaxDispatchStrategy implements DispatchStrategy
{
    public function fire(array $info): Response
    {
        if (!is_callable($info[1])) {
            throw new \Exception('The Ajax dispatcher should be passed a callable.');
        }

        return response()->setContent(
            Container::call($info[1])
        );
    }
}
