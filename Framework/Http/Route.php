<?php

namespace Framework\Http;

use Closure;
use Framework\Container;
use Framework\Facades\Serialize;
use Framework\Middleware\Middleware;
use Serializable;

class Route implements Serializable
{
    /**
     * The HTTP request method.
     *
     * @var string
     */
    public $method;

    /**
     * The url for this route.
     *
     * @var string
     */
    public $url;

    /**
     * The action to undertake if this route is hit.
     *
     * @var string|Closure
     */
    public $action;

    /**
     * A array containing all defined route middleware.
     *
     * @var array
     */
    public $middleware;

    /**
     * Route constructor.
     *
     * @param string         $method
     * @param string         $url
     * @param string|Closure $action
     * @param array          $middleware
     */
    public function __construct(string $method, string $url, $action, array $middleware = [])
    {
        $this->method = $method;
        $this->url = $url;
        $this->action = $action;
        $this->middleware = $middleware;
    }

    /**
     * Run all middleware that is defined on the route.
     * Returns true if all middleware succeeded, the response otherwise.
     *
     * @return bool|\Framework\Http\Response
     *
     * @throws \Exception
     */
    public function runMiddleware()
    {
        foreach ($this->middleware as $middleware) {
            switch ($middleware->run()) {
                case Middleware::Success:
                    continue;
                    break;

                case Middleware::Abort:
                    return Container::get(Response::class);
                    break;

                default:
                    throw new \Exception('Invalid Middleware result received! Middleware should return a result.');
                    break;
            }
        }

        return true;
    }

    /**
     * Returns a formatted string containing all defined middleware for printing.
     *
     * @return string
     */
    public function getMiddlewareAsString(): string
    {
        if ($this->middleware === []) {
            return '';
        }

        $ret = '';

        foreach ($this->middleware as $mw) {
            $classname = $mw->getClassname();
            $ret .= basename($classname).'('.$mw->getFormattedArguments().'), ';
        }

        return rtrim($ret, ', ');
    }

    /**
     * String representation of object.
     *
     * @link http://php.net/manual/en/serializable.serialize.php
     *
     * @return string the string representation of the object or null
     *
     * @since 5.1.0
     */
    public function serialize()
    {
        $data =
        [
            $this->method,
            $this->url,
            Serialize::do($this->action),
            is_callable($this->action),
            $this->middleware,
        ];

        return serialize($data);
    }

    /**
     * Constructs the object.
     *
     * @link http://php.net/manual/en/serializable.unserialize.php
     *
     * @param string $serialized <p>
     *                           The string representation of the object.
     *                           </p>
     *
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        $data = unserialize($serialized);

        $this->method = $data[0];
        $this->url = $data[1];
        $this->action = Serialize::undo($data[2], $data[3]);
        $this->middleware = $data[4];
    }
}
