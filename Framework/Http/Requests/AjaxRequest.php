<?php

namespace Framework\Http\Requests;

use Framework\Http\Request;

class AjaxRequest extends Request
{
    /**
     * If this request is not an ajax request, simply return to the previous page.
     *
     * AjaxRequest constructor.
     */
    public function __construct()
    {
        parent::__construct();

        if (!$this->isAjax()) {
            response()->withInput()->back();
        }
    }
}
