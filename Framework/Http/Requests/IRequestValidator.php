<?php

namespace Framework\Http\Requests;

interface IRequestValidator
{
    public function rules(): array;
    public function filters(): array;
    public function fieldNames(): array;
}
