<?php

namespace Framework\Http\Requests;

use Framework\Http\Requests\Traits\CustomValidateTrait;
use Framework\Traits\Expandable;
use Framework\Traits\IExpandable;
use GUMP;
use function Stringy\create as s;

class DynamicValidator extends GUMP implements IExpandable
{
    use CustomValidateTrait, Expandable;

    /**
     * GUMP does not allow access to the given variables by default,
     * so we save the data in a self-hosted array.
     *
     * This is required for validators that depend on other fields, like require_if.
     *
     * @var array
     */
    protected $given_data;

    /**
     * Register a filter or validator.
     *
     * @param string   $name
     * @param callable $extension
     *
     * @throws \Exception
     */
    public function registerExtension(string $name, $extension)
    {
        if (!s($name)->startsWith('filter_') && !s($name)->startsWith('validate_')) {
            throw new \Exception('A validation extensions name should start with filter_ or validate_');
        }

        if (!is_callable($extension)) {
            throw new \Exception("The extension for $name should be a callable. Does the function exists and is it public?");
        }

        $this->extensions[$name] = $extension;
    }

    /**
     * Unregister a filter or validator.
     *
     * @param string $name
     */
    public function unregisterExtension(string $name)
    {
        unset($this->extensions[$name]);
    }

    /**
     * Setter for given_data.
     *
     * @param array $givenData
     */
    public function setGivenData(array $givenData)
    {
        $this->given_data = $givenData;
    }

    /**
     * Process the validation errors and return an array of errors with field names as keys.
     *
     * @param $convert_to_string
     *
     * @return array | null (if empty)
     */
    public function get_errors_array($convert_to_string = null)
    {
        if (empty($this->errors)) {
            return ($convert_to_string) ? null : array();
        }

        $response = [];

        foreach ($this->errors as $error) {
            $field = $error['field'];
            $param = $error['param'];

            // Let's fetch explicit field names if they exist else create field name
            if (array_key_exists($field, self::$fields)) {
                $fieldName = self::$fields[$error['field']];
            } else {
                $fieldName = ucwords(str_replace(array('_', '-'), chr(32), $error['field']));
            }

            switch ($error['rule']) {
                case 'mismatch':                $response[$field] = "Er is geen validatieregel voor $fieldName."; break;
                case 'validate_required':       $response[$field] = "$fieldName is een verplicht veld."; break;
                case 'validate_valid_email':    $response[$field] = "$fieldName moet een correct E-mail adres zijn."; break;
                case 'validate_max_len':        $response[$field] = "$fieldName moet $param tekens of minder zijn."; break;
                case 'validate_min_len':        $response[$field] = "$fieldName moet $param tekens of meer zijn."; break;
                case 'validate_exact_len':      $response[$field] = "$fieldName moet $param tekens lang zijn."; break;
                case 'validate_alpha':          $response[$field] = "$fieldName mag alleen alpha karakters bevatten (a-z, A-Z)."; break;
                case 'validate_alpha_numeric':  $response[$field] = "$fieldName mag alleen alpha-nummerieke karakters bevatten (a-z, A-Z, 0-9)."; break;
                case 'validate_alpha_dash':     $response[$field] = "$fieldName mag alleen alpha karakters &amp; streepjes bevatten (a-z, A-Z, 0-9, _-)."; break;
                case 'validate_numeric':        $response[$field] = "$fieldName mag alleen nummerieke karakters bevatten."; break;
                case 'validate_integer':        $response[$field] = "$fieldName mag alleen een nummerieke waarde zijn."; break;
                case 'validate_boolean':        $response[$field] = "$fieldName mag alleen een true of false waarde zijn."; break;
                case 'validate_float':          $response[$field] = "$fieldName mag alleen een zwevendekommagetal zijn."; break;
                case 'validate_valid_url':      $response[$field] = "$fieldName moet een geldige url zijn."; break;
                case 'validate_url_exists':     $response[$field] = "$fieldName URL bestaat niet."; break;
                case 'validate_valid_ip':       $response[$field] = "$fieldName moet een geldig IP-adres zijn."; break;
                case 'validate_valid_cc':       $response[$field] = "$fieldName moet een geldig credit card nummer zijn."; break;
                case 'validate_valid_name':     $response[$field] = "$fieldName moet een geldige naam zijn."; break;
                case 'validate_contains':       $response[$field] = "$fieldName moet de volgende waardes bevatten: ".implode(', ', $param).'.'; break;
                case 'validate_street_address': $response[$field] = "$fieldName moet een geldige straatnaam adres zijn."; break;
                case 'validate_date':           $response[$field] = "$fieldName moet een geldige datum zijn."; break;
                case 'validate_min_numeric':    $response[$field] = "$fieldName moet een nummerieke waarde zijn, gelijk aan, of groter dan $param."; break;
                case 'validate_max_numeric':    $response[$field] = "$fieldName moet een nummerieke waarde zijn, gelijk aan, of kleiner dan $param"; break;
                case 'validate_min_age':        $response[$field] = "$fieldName moet een leeftijd zijn, gelijk aan, of groter dan $param"; break;
                case 'validate_custom_date':    $response[$field] = "$fieldName moet een datum van het formaat $param zijn."; break;
                case 'validate_time':           $response[$field] = "$fieldName moet een geldige tijd zijn."; break;
                case 'validate_unique':         $response[$field] = "$fieldName moet een unieke waarde bevatten."; break;
                case 'validate_require_if':     $response[$field] = "$fieldName is een verplicht veld."; break;
                case 'validate_exists':         $response[$field] = "$fieldName bestaat niet (meer)."; break;
                case 'validate_file':           $response[$field] = "$fieldName moet een geldig bestand zijn."; break;
                case 'validate_image':          $response[$field] = "$fieldName moet een afbeelding zijn."; break;
                case 'validate_confirmed':      $response[$field] = "$fieldName moet gelijk zijn aan het bevestigingsveld.";
                    $response[$field.'_confirmation'] = ''; // create error key in array.
                    break;
                case 'validate_different':      $response[$field] = "$fieldName mag niet gelijk zijn aan ".strtolower(self::$fields[$param]).'.'; break;
                default: $response[$field] = "$fieldName is incorrect";
            }
        }

        return $response;
    }

    /**
     * Format the IBAN number.
     *
     * @param string $value
     * @param array  $params
     *
     * @return string
     */
    protected function filter_iban($value, $params = null)
    {
        return implode(' ', str_split(strtoupper(preg_replace('/\s+/', '', $value)), 4));
    }

    /**
     * Filter to convert the given string to uppercase.
     *
     * @param string $value
     * @param array  $params
     *
     * @return string
     */
    protected function filter_upper($value, $params = null)
    {
        return s($value)->toUpperCase();
    }

    /**
     * Filter to convert the given string to lowercase.
     *
     * @param string $value
     * @param array  $params
     *
     * @return string
     */
    protected function filter_lower($value, $params = null)
    {
        return s($value)->toLowerCase();
    }

    /**
     * @param string $date
     * @param string $params
     *
     * @return string
     */
    protected function filter_date(string $date, $params = null)
    {
        $format = $params[0] ?? 'Y-m-d';

        return (new Carbon($date))->format($format);
    }

    /**
     * @param string $date
     * @param string $params
     *
     * @return string
     */
    protected function filter_timestamp(string $date, $params = null)
    {
        return (new Carbon($date))->format('Y-m-d H:i:s');
    }
}
