<?php

namespace Framework\Http\Requests\Traits;

use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberUtil;

trait CustomValidateTrait
{
    /**
     * Determine if the provided input is a valid date (ISO 8601).
     *
     * Usage: '<index>' => 'date'
     *
     * @param string $field
     * @param string $input date ('Y-m-d') or datetime ('Y-m-d H:i:s')
     * @param string $param New this parameter can specify the PHP date format
     *
     * @return mixed
     */
    protected function validate_custom_date($field, $input, $param = null)
    {
        if (!isset($input[$field]) || empty($input[$field])) {
            return null;
        }

        $date = \DateTime::createFromFormat($param, $input[$field]);
        if ($date === false || $input[$field] != date($param, $date->getTimestamp())) {
            return [
                'field' => $field,
                'value' => $input[$field],
                'rule' => __FUNCTION__,
                'param' => $param,
            ];
        }
    }

    /**
     * @param $field
     * @param $input
     * @param null $param
     *
     * @return array|null
     */
    protected function validate_time($field, $input, $param = null)
    {
        if (!isset($input[$field]) || empty($input[$field])) {
            return null;
        }

        $date = \DateTime::createFromFormat('H:i', $input[$field]);
        if ($date === false || $input[$field] != date('H:i', $date->getTimestamp())) {
            return [
                'field' => $field,
                'value' => $input[$field],
                'rule' => __FUNCTION__,
                'param' => $param,
            ];
        }
    }

    /*
     * Validate a phone number using libphonenumber
     * This overwrites the default GUMP phone validator because it is not sufficient.
     */
    public function validate_phone_number($field, $input, $param = null)
    {
        if (!isset($input[$field]) || empty($input[$field])) {
            return null;
        }

        $number = null;
        $util = PhoneNumberUtil::getInstance();

        try {
            $number = $util->parse($input[$field], 'NL');
        } catch (NumberParseException $e) {
            return [
                'field' => $field,
                'value' => $input[$field],
                'rule' => __FUNCTION__,
                'param' => $param,
            ];
        }

        if ($util->isValidNumber($number)) {
            return null;
        }

        return [
            'field' => $field,
            'value' => $input[$field],
            'rule' => __FUNCTION__,
            'param' => $param,
        ];
    }

    /**
     * Conditional require validator.
     * When the passed condition evaluates to true, a require check is done.
     * When the passed condition evaluates to false, the validation succeeds whether the
     * field is empty or not.
     *
     * Example usage:
     * 'require_if,id:eq:5'
     * 'require_if,id:gt:95'
     * 'require_if,boolean_field:eq:true'
     * 'require_if,location:eq:set'
     * 'require_if,location:neq:set'
     *
     * Normally, the value field is resolved to a string, integer, float or boolean.
     * If this cannot be solved using loose evaluation (!= or ==) it can be disabled with a 4th parameter.
     *
     * Example usage:
     *
     * 'require_if,accept_field:eq:yes:false' <- normally 'yes' would be converted to boolean. Passing 'false'
     * as a 4th parameter prevents this.
     * Please note that this disables type resolution for both sides of the comparison.
     *
     * @param $field
     * @param $input
     * @param null $param
     *
     * @return array|null|void
     *
     * @throws \Exception
     */
    protected function validate_require_if($field, $input, $param = null)
    {
        if ($param === null) {
            throw new \Exception('Require_if parameter cannot be empty.');
        }

        // Validate parameter count.
        $param_array = explode(':', $param);
        if (sizeof($param_array) < 3 || sizeof($param_array) > 4) {
            throw new \Exception("Require_if requires three parameters, which are seperated with ':'.");
        }

        /*
         * 4th parameter not specified, so type resolution is requested.
         * list(...) will fail if $param_array does not contain four elements, so we append true.
         */
        if (sizeof($param_array) === 3) {
            $param_array[] = true;
        }

        // Continue with variables.
        list($reference, $operator, $comparator, $resolve_type) = $param_array;
        unset($param_array);

        /*
         * The set validators:
         * 'require_if,location:eq:set'
         * 'require_if,location:neq:set'
         */
        if ($comparator === 'set') {
            if ($operator === 'eq') { // Check if set
                if (isset($this->given_data[$reference])
                    && !empty($this->given_data[$reference])) {
                    return null;
                }
            } elseif ($operator === 'neq') { // Check if not set
                if (!isset($this->given_data[$reference])
                    || empty($this->given_data[$reference])) {
                    return null;
                }
            } else { // You dun goofed
                throw new \Exception("The operator $operator is not supported when using the 'set' condition. Use eq or neq.");
            }

            return [
                'field' => $field,
                'value' => $input[$field],
                'rule' => __FUNCTION__,
                'param' => $param,
            ];
        }

        // Resolve the comparator's type (right hand side).
        $resolved_comparator = $resolve_type
            ? $this->string_to_type($comparator)
            : $comparator;

        // Resolve the reference type (left hand side).
        $resolved_reference = $resolve_type
            ? $this->string_to_type($this->given_data[$reference])
            : $this->given_data[$reference];

        /*
        * Example value validators:
        * 'require_if,id:eq:5'
        * 'require_if,id:gt:95'
        * 'require_if,boolean_field:eq:true'
        */
        $execute_check = false;

        switch ($operator) {
            case 'gt':     // Greater than
                if ($resolved_reference > $resolved_comparator) {
                    $execute_check = true;
                }
                break;
            case 'lt':     // Lesser than
                if ($resolved_reference < $resolved_comparator) {
                    $execute_check = true;
                }
                break;
            case 'ge':     // Greater than or equal too
                if ($resolved_reference >= $resolved_comparator) {
                    $execute_check = true;
                }
                break;
            case 'le':     // Lesser than or equal too
                if ($resolved_reference <= $resolved_comparator) {
                    $execute_check = true;
                }
                break;
            case 'eq':     // Equal
                if ($resolved_reference === $resolved_comparator) {
                    $execute_check = true;
                }
                break;
            case 'leq':    // Loosely equal
                if ($resolved_reference == $resolved_comparator) {
                    $execute_check = true;
                }
                break;
            case 'neq':    // Not equal
                if ($resolved_reference !== $resolved_comparator) {
                    $execute_check = true;
                }
                break;
            case 'lneq':   // Loosely not equal
                if ($resolved_reference != $resolved_comparator) {
                    $execute_check = true;
                }
                break;
            default:       // Operator not found
                throw new \Exception("The operator $operator is not a valid operator for require_if.");
                break;
        }

        /*
         * Condition for require was not met,
         * so the validation succeeds.
         */
        if (!$execute_check) {
            return null;
        }

        /*
         * Check should be executed.
         */
        else {
            if (isset($input[$field]) && !empty($input[$field])) {
                return null;
            }
        }

        /*
         * Require condition was met, but the field was empty.
         * This means the validation failed.
         */
        return [
            'field' => $field,
            'value' => $input[$field],
            'rule' => __FUNCTION__,
            'param' => $param,
        ];
    }

    private function string_to_type($var)
    {
        // Boolean
        if (in_array($var, ['true', 'false', '1', '0', 'yes', 'no'], true)) {
            return filter_var($var, FILTER_VALIDATE_BOOLEAN);
        }

        // Integer or floating point
        elseif (is_numeric($var)) {
            return $var + 0;
        } // Trick to convert number to integer or float.

        // String
        return $var;
    }

    /**
     * Check if the file actually has been uploaded.
     * This validator also supports an array of files.
     *
     * @param $field
     * @param $input
     * @param null $param
     */
    protected function validate_file($field, $input, $param = null)
    {
        if (!isset($input[$field]) || empty($input[$field])) {
            return null;
        }

        if (!is_array($_FILES[$field]['tmp_name'])) {
            if (!file_exists($_FILES[$field]['tmp_name'])
            || !is_uploaded_file($_FILES[$field]['tmp_name'])) {
                return [
                    'field' => $field,
                    'value' => $input[$field],
                    'rule' => __FUNCTION__,
                    'param' => $param,
                ];
            }
        } else {
            for ($i = 0; $i < sizeof($_FILES[$field]['name']); ++$i) {
                if (!file_exists($_FILES[$field]['tmp_name'][$i])
                    || !is_uploaded_file($_FILES[$field]['tmp_name'][$i])) {
                    return [
                        'field' => $field,
                        'value' => $input[$field],
                        'rule' => __FUNCTION__,
                        'param' => $param,
                    ];
                }
            }
        }

        return null;
    }

    /**
     * Check if the given file is an image.
     * Allowed image types:
     * .gif
     * .jpeg / .jpg
     * .png
     * .bmp
     * .ico
     * .tiff.
     *
     * @param $field
     * @param $input
     * @param null $param
     *
     * @return array|null
     */
    protected function validate_image($field, $input, $param = null)
    {
        if (!isset($input[$field]) || empty($input[$field])) {
            return null;
        }

        $allowed_types =
        [
            IMAGETYPE_GIF,
            IMAGETYPE_JPEG,
            IMAGETYPE_PNG,
            IMAGETYPE_BMP,
            IMAGETYPE_ICO,
            IMAGETYPE_TIFF_II,
            IMAGETYPE_TIFF_MM,
        ];

        if (!is_array($_FILES[$field]['name'])) {
            $detected_type = exif_imagetype($_FILES[$field]['tmp_name']);

            // Check if the detected type is one of the allowed ones.
            if (in_array($detected_type, $allowed_types)) {
                return null;
            }

            // Wrong type; error.
            return [
                'field' => $field,
                'value' => $input[$field],
                'rule' => __FUNCTION__,
                'param' => $param,
            ];
        } else {
            for ($i = 0; $i < sizeof($_FILES[$field]['name']); ++$i) {
                $detected_type = exif_imagetype($_FILES[$field]['tmp_name'][$i]);

                // Check if the detected type is one of the allowed ones.
                if (in_array($detected_type, $allowed_types)) {
                    continue;
                }

                // Wrong type; error.
                return [
                    'field' => $field,
                    'value' => $input[$field],
                    'rule' => __FUNCTION__,
                    'param' => $param,
                ];
            }

            return null;
        }
    }

    /**
     * @param $field
     * @param $input
     * @param null $param
     */
    protected function validate_confirmed($field, $input, $param = null)
    {
        if (!isset($input[$field]) || empty($input[$field])) {
            return null;
        }

        if ($input[$field] !== $input[$field.'_confirmation']) {
            return [
                'field' => $field,
                'value' => $input[$field],
                'rule' => __FUNCTION__,
                'param' => $param,
            ];
        }

        return null;
    }

    /**
     * @param $field
     * @param $input
     * @param null $param
     */
    protected function validate_different($field, $input, $param = null)
    {
        if (!isset($input[$field]) || empty($input[$field])) {
            return null;
        }

        if ($input[$field] === $input[$param]) {
            return [
                'field' => $field,
                'value' => $input[$field],
                'rule' => __FUNCTION__,
                'param' => $param,
            ];
        }

        return null;
    }
}
