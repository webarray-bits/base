<?php

namespace Framework\Http\Requests;

use Framework\Container;
use Framework\Facades\Input;
use Framework\Http\Request;
use Framework\Http\Response;

/**
 * Class RequestValidator.
 */
abstract class RequestValidator extends Request
{
    /**
     * Array containing all errors found.
     *
     * @var array
     */
    private $errors;

    /**
     * The validator.
     *
     * @var DynamicValidator
     */
    private $validator;

    abstract public function rules(): array;
    abstract public function filters(): array;
    abstract public function fieldNames(): array;

    public function success(): array
    {
        return [];
    }

    protected function response(): Response
    {
        return response()
            ->json($this->errors)
            ->setStatusCode(406);
    }

    /**
     * RequestValidator constructor.
     */
    public function __construct()
    {
        parent::__construct();

        /*
         * Because the ServiceRegistrar registers validators
         * that services provide, the validator is resolved from the
         * container.
         */
        $this->validator = Container::get(DynamicValidator::class);

        if ($this->validate()) {
            response()->json($this->success());

            return;
        }

        $this->setErrors();

        if (request()->isAjax()) {
            $this->response()->send();
            die;
        }

        response()->withInput()->back();
    }

    private function validate(): bool
    {
        /*
         * These functions already check if the given array is null or empty;
         * therefore we don't need to check that ourselves.
         */
        $this->validator->validation_rules($this->rules());
        $this->validator->filter_rules($this->filters());
        $this->validator->set_field_names($this->fieldNames());

        /*
         * Collect all variables that are defined in the current request url and prepend their
         * keys with Url.
         * If Url.$key already exists, an error is thrown as this could create hard-to-find bugs.
         */
        $vars = [];
        foreach (request()->variables() as $key => $value) {
            if (Input::exists('Url.'.$key)) {
                throw new \Exception("Input contains Url.$key and the route contains $key as a variable, so I don't know what to validate!");
            }

            $vars['Url.'.ucfirst($key)] = $value;
        }

        /*
         * GUMP can't validate on array's, so we can't just array_merge
         * the $_FILES array for file validation.
         *
         * Instead, we iterate over the keys and put the key (which is the input name)
         * as the value too. This way it will play nice with GUMP.
         */

        $files = [];
        foreach ($_FILES as $key => $value) {
            $files[$key] = $key;
        }

        /*
         * Validation is done on all input variables and
         * the variables defined in the current request url.
         */
        $data = array_merge(Input::getArray(), $vars, $files);

        /*
         * Validate all the things.
         */
        $this->validator->setGivenData($data);
        $result = $this->validator->run($data);

        if ($result !== false) {
            Input::merge($result);

            return true;
        }

        $this->errors = $this->validator->get_errors_array();

        return false;
    }

    /**
     * if there are custom errors this function will overwrite the original errors.
     * Else it will place the errors in the messages array.
     */
    private function setErrors()
    {
        if (method_exists($this, 'errors')) {
            $this->errors = ['messages' => array_merge($this->errors, array_intersect_key($this->errors(), $this->errors))];
        } else {
            $this->errors = ['messages' => $this->errors];
        }
    }
}
