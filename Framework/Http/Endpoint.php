<?php

namespace Framework\Http;

use Closure;
use Framework\Container;
use Framework\Middleware\Proxy;

class Endpoint
{
    /**
     * The current prefix.
     *
     * @var string
     */
    private static $prefix = '';

    /**
     * The current middleware.
     *
     * @var array
     */
    private static $middleware = [];

    /**
     * The route registry instance.
     *
     * @var RouteRegistry
     */
    private static $registry = null;

    private static function registerRoute($method, $url, $action, $middleware)
    {
        if (self::$registry == null) {
            self::$registry = Container::get(RouteRegistry::class);
        }

        self::$registry->register(
            $method,
            self::$prefix.$url,
            $action,
            array_merge(self::$middleware, self::resolveMiddleware($middleware))
        );
    }

    private static function resolveMiddleware($middleware)
    {
        if ($middleware === []) {
            return [];
        } else if ($middleware instanceof Proxy) {
            return [$middleware];
        } else if (is_array($middleware)) {
            foreach ($middleware as $item) {
                $result[] = self::resolveMiddleware($item);
            }

            return $result;
        } else if (is_string($middleware)) {
            return [middleware($middleware)];
        } else {
            throw new \Exception('Middleware can be a string, a middleware proxy object or an array containing these.');
        }
    }

    /**
     * Takes an array of options and a closure.
     * All the options defined will be applied to the routes and groups nested within the closure.
     * Used in App/routes.php.
     *
     * @param array   $options
     * @param Closure $function
     *
     * @internal param $closure
     */
    public static function group(array $options, Closure $function)
    {
        $middleware = array_key_exists('middleware', $options);
        $prefix = array_key_exists('prefix', $options);

        // If there is a prefix, append it.
        if ($prefix) {
            self::$prefix .= $options['prefix'];
        }

        // If middleware was specified for this group, add it to the current list of middleware.
        if ($middleware) {
            self::$middleware = array_merge(self::$middleware, self::resolveMiddleware($options['middleware']));
        }

        // Register all routes / groups within this group.
        $function();

        // If there was middleware specified, remove it again.
        if ($middleware) {
            foreach (self::$middleware as $key => $value) {
                unset(self::$middleware[$key]);
            }
        }

        // If there was a prefix specified, remove it again.
        if ($prefix) {
            self::$prefix = str_replace($options['prefix'], '', self::$prefix);
        }
    }

    /**
     * Define a get route. Used in App/routes.php.
     *
     * @param string $url
     * @param $action
     * @param array|string $middleware
     */
    public static function get(string $url, $action, $middleware = [])
    {
        self::registerRoute('GET', $url, $action, $middleware);
    }

    /**
     * Define a put route. Used in App/routes.php.
     *
     * @param string $url
     * @param $action
     * @param array|string $middleware
     */
    public static function put(string $url, $action, $middleware = [])
    {
        self::registerRoute('PUT', $url, $action, $middleware);
    }

    /**
     * Define a patch route. Used in App/routes.php.
     *
     * @param string $url
     * @param $action
     * @param array|string $middleware
     */
    public static function patch(string $url, $action, $middleware = [])
    {
        self::registerRoute('PATCH', $url, $action, $middleware);
    }

    /**
     * Define a post route. Used in App/routes.php.
     *
     * @param string $url
     * @param $action
     * @param array|string $middleware
     */
    public static function post(string $url, $action, $middleware = [])
    {
        self::registerRoute('POST', $url, $action, $middleware);
    }

    /**
     * Define a delete route. Used in App/routes.php.
     *
     * @param string $url
     * @param $action
     * @param array|string $middleware
     */
    public static function delete(string $url, $action, $middleware = [])
    {
        self::registerRoute('DELETE', $url, $action, $middleware);
    }
}
