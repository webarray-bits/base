<?php

namespace Framework\Http;

use FastRoute\DataGenerator\GroupCountBased as Generator;
use FastRoute\Dispatcher\GroupCountBased as InternalDispatcher;
use FastRoute\RouteParser\Std;

class Dispatcher
{
    /**
     * Returns the route handler and variables for the given method and uri.
     * The third argument is the routes that should be parsed.
     * The structure for each route is as follows:
     * [
     *  string $method,
     *  string $route,
     *  string|callable|Response $handler
     * ]
     *
     * This will return an array containing the results. The array is in the following format:
     * [
     *  int $code,
     *  string|callable|Response handler
     *  array variables ($key => $value pairs)
     * ]
     *
     * The returned codes correspond to the following:
     * 0 => FastRoute\Dispatcher::NOT_FOUND
     * 1 => FastRoute\Dispatcher::FOUND
     * 2 => FastRoute\Dispatcher::METHOD_NOT_ALLOWED
     *
     * @param string $method
     * @param string $uri
     * @param array $routes
     * @return array
     */
    public static function dispatch(string $method, string $uri, array $routes)
    {
        $parser = new Std();
        $generator = new Generator();

        foreach ($routes as $definition) {
            $dataInstances = $parser->parse($definition[1]); // [1] = route

            foreach ((array)$definition[0] as $definitionMethod) { // [0] = method
                foreach ($dataInstances as $data) {
                    $generator->addRoute($definitionMethod, $data, $definition[2]); // [2] = Handler (string|callable|Response)
                }
            }
        }

        return (new InternalDispatcher(
            $generator->getData()
        ))->dispatch($method, $uri);
    }
}