<?php

namespace Framework\Http;

use Framework\Container;
use Framework\Data\CookieManager;
use Framework\Data\InputManager;
use Framework\Data\SessionManager;
use Framework\Facades\Cookie;
use Framework\Facades\Input;
use Framework\Facades\Session;
use Framework\Data\Storage\UploadedFile;

class Request
{
    /**
     * The current request method.
     *
     * @var string
     */
    private $method;

    /**
     * The current request URI.
     *
     * @var string
     */
    private $uri;

    /**
     * The called controller.
     * Lazy loaded.
     *
     * @var string
     */
    private $controller;

    /**
     * The function called on the controller.
     * Lazy loaded.
     *
     * @var string
     */
    private $action;

    /**
     * Is the current request an ajax request or not.
     * Lazy loaded.
     *
     * @var bool
     */
    private $ajax;

    /**
     * All variables that are defined in the url (ex. /event/{key}/edit).
     * Variables defined as key => value.
     *
     * @var array
     */
    private $variables = [];

    /**
     * Request constructor.
     */
    public function __construct()
    {
        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->uri = rawurldecode(
            parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)
        );

        $this->middleware = null;
    }

    /**
     * Get input from the request.
     *
     * @param $name
     *
     * @return mixed
     */
    public function __get($name)
    {
        if (Input::fileExists($name)) {
            return Input::file($name);
        }

        if (Input::exists($name)) {
            return Input::get($name);
        }

        return null;
    }

    /**
     * If the current request is on an controller action,
     * return the name of the controller.
     * Otherwise return false.
     */
    public function controller()
    {
        if (isset($this->controller)) {
            return $this->controller;
        }

        $action = Container::get(RouteRegistry::class)->getUrlAction($this->uri);

        if ($action === '' || $action === 'Callable') {
            return false;
        }

        $this->controller = explode('@', $action)[0];

        return $this->controller;
    }

    /**
     * If the current request is on an controller action,
     * return the name of the called function.
     * Otherwise return false.
     */
    public function action()
    {
        if (isset($this->action)) {
            return $this->action;
        }

        $action = Container::get(RouteRegistry::class)->getUrlAction($this->uri, $this->method);

        if ($action === '' || $action === 'Callable') {
            return false;
        }

        $this->action = explode('@', $action)[1];

        return $this->action;
    }

    /**
     * @return string
     */
    public function method()
    {
        return $this->method;
    }

    /**
     * Checks if the given method matches the current method.
     *
     * @param string $method
     *
     * @return bool
     */
    public function isMethod(string $method): bool
    {
        return $this->method === $method;
    }

    /**
     * @return string
     */
    public function uri()
    {
        return $this->uri;
    }

    /**
     * Checks if the given uri matches the current uri.
     * A wildcard can be used. If the current uri is /admin/panel and /admin/* is passed,
     * this function will return true.
     *
     * @param string $pattern
     *
     * @return bool
     */
    public function is(string $pattern): bool
    {
        if ($this->uri === $pattern) {
            return true;
        }

        $pattern = preg_quote($pattern, '#');
        $pattern = str_replace('\*', '.*', $pattern);

        return (bool) preg_match('#^'.$pattern.'\z#u', $this->uri);
    }

    /**
     * Retrieve input.
     *
     * @param string $key
     * @param null   $default
     *
     * @return mixed|InputManager|null
     */
    public function input(string $key = null, $default = null)
    {
        if ($key === null) {
            return Container::get(InputManager::class);
        }

        return Input::get($key, $default);
    }

    /**
     * Retrieve a file from input.
     * If multiple files are uploaded using the same input (using 'multiple' on the file input),
     * a index should be specified to retrieve te right file.
     *
     * @param string $key
     * @param null   $index
     *
     * @return bool|UploadedFile
     */
    public function file(string $key, $index = null)
    {
        return Input::file($key, $index);
    }

    /**
     * Retrieve all input.
     *
     * @return array
     */
    public function all()
    {
        return Input::getArray();
    }

    /**
     * Retrieve a variable defined in the url.
     *
     * @param string $key
     *
     * @return bool|mixed
     */
    public function variable(string $key)
    {
        if (!array_key_exists($key, $this->variables)) {
            return false;
        }

        return $this->variables[$key];
    }

    /**
     * Retrieve or set all variables defined in the url.
     *
     * @param array $vars
     *
     * @return array|void
     */
    public function variables(array $vars = [])
    {
        if ($vars === []) {
            return $this->variables;
        }

        $this->variables = $vars;
    }

    /**
     * Retrieve a cookie.
     *
     * @param string $key
     *
     * @return mixed|CookieManager|null
     */
    public function cookie(string $key = null)
    {
        if ($key === null) {
            return Container::get(CookieManager::class);
        }

        return Cookie::get($key);
    }

    /**
     * Returns the session manager from the container.
     *
     * @param null $key
     *
     * @return SessionManager|mixed|null
     */
    public function session($key = null)
    {
        if ($key === null) {
            return Container::get(SessionManager::class);
        }

        return Session::get($key);
    }

    /**
     * Shortcut method for the environment variables.
     *
     * @param string $key
     * @param null   $default
     *
     * @return mixed
     */
    public function env(string $key, $default = null)
    {
        return env($key, $default);
    }

    /**
     * Returns whether the current request is an ajax request.
     *
     * @return bool
     */
    public function isAjax(): bool
    {
        if (isset($this->ajax)) {
            return $this->ajax;
        }

        $this->ajax =
            strtolower(filter_input(INPUT_SERVER, 'HTTP_X_REQUESTED_WITH')) === 'xmlhttprequest' // Standard AJAX check
        && !array_key_exists('X-Request-Type', apache_request_headers()); // Check for turbolinks with custom header set

        return $this->ajax;
    }
}
