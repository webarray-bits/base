<?php

namespace Framework\Http;

use FastRoute;
use Framework\Container;
use Framework\Controller\Controller;
use Framework\Event\Http\HttpDispatchEvent;
use Framework\Facades\Input;
use Framework\Http\Strategy\AjaxDispatchStrategy;
use Framework\Http\Strategy\WebDispatchStrategy;
use Framework\Service\ServiceRegistrar;
use Whoops\Exception\ErrorException;

class Router
{
    /**
     * @return Response
     *
     * @throws ErrorException
     */
    public function handleRequest()
    {
        /*
         * In forms, the _METHOD hidden input is used to spoof the PUT / PATCH / DELETE methods.
         */
        if (request()->method() === 'POST' && Input::exists('_METHOD')) {
            $method = $_POST['_METHOD'];
        } else {
            $method = request()->method();
        }

        $info = Dispatcher::dispatch(
            $method,
            request()->uri(),
            Container::get(RouteRegistry::class)->collect()
        );

        if ($info[0] === FastRoute\Dispatcher::NOT_FOUND) {
            return response()->error(404, 404);
        } else if ($info[0] === FastRoute\Dispatcher::METHOD_NOT_ALLOWED) {
            return response()->error(405, 405);
        }

        /**
         * First, we check if there is middleware to run on this route.
         *
         * @var array
         */
        $routes = Container::get(RouteRegistry::class)->getRoutes();

        /**
         * The current route.
         *
         * @var Route
         */
        $this_route = null;

        foreach ($routes as $route) {
            /*
             * Compare actions because routes can contain wildcards.
             * Closures can be compared.
             */
            if ($info[1] === $route->action) {
                $middlewareResult = $route->runMiddleware();

                // We must check for true, not a truthy value.
                if ($middlewareResult === true) {
                    $this_route = $route;
                    break;
                }

                return $middlewareResult;
            }
        }

        $response = null;

        /*
         * Check if the action is an string (ex. PageController@index) or a closure.
         * If it is a string, invoke the controller.
         * If it is a closure, use PHP-DI to call the closure.
         * This will handle dependency injection for the closure.
         */
        if (!request()->isAjax()) {
            $_SESSION['previous_url'] = request()->uri();
        }

        // Dispatch the HttpDispatchEvent with the current route;
        Container::get(ServiceRegistrar::class)->dispatch(
            'http.dispatch',
            new HttpDispatchEvent($this_route)
        );

        if ($info[1] instanceof Response) {
            return $info[1];
        } else if (is_string($info[1])) {
            return (new WebDispatchStrategy())->fire($info);
        } else {
            return (new AjaxDispatchStrategy())->fire($info);
        }
    }
}
