<?php

namespace Framework\Http;

use FastRoute;
use Framework\Config;
use Framework\Facades\Cache;
use function Stringy\create as s;

class RouteRegistry
{
    /**
     * One-dimensional, numerically indexed array with all configured routes.
     *
     * @var array
     */
    private $routes = [];

    /**
     * All defined actions.
     *
     * @var array
     */
    private $actions = [];

    /**
     * The current index.
     *
     * @var int
     */
    private $index = 0;

    /**
     * Register a route within the route registry.
     *
     * @param string|Route $method
     * @param $url
     * @param $action
     * @param array $middleware
     *
     * @throws \Exception
     */
    public function register($method, $url = null, $action = null, array $middleware = [])
    {
        if ($method instanceof Route) { // A route is given
            $this->routes[$this->index] = $method; // $method is filled with a Route instance

            if (!is_callable($method->action)) {
                if (array_key_exists($method->action, $this->actions)) {
                    throw new \Exception("Only one route per controller action is allowed. $url - $method -> ".$method->action);
                }

                $this->actions[$method->action] = $this->index;
            }
        } elseif ($url !== null && $action !== null) { // A normal registration is being done
            $this->routes[$this->index] = new Route($method, $url, $action, $middleware);

            if (!is_callable($action)) {
                if (array_key_exists($action, $this->actions)) {
                    throw new \Exception("Only one route per controller action is allowed. $url - $method -> $action");
                }

                $this->actions[$action] = $this->index;
            }
        } else { // Invalid arguments
            throw new \Exception('The register function should either receive an Framework\\Http\\Route instance, or a normal route registration.');
        }

        ++$this->index;
    }

    /**
     * Get all configured routes, and feed them to the FastRoute RouteCollector.
     * This function also handles route caching (if enabled).
     */
    public function collect()
    {
        if (config('Cache.Routes') && cache()->has(config('Cache.RoutesName'))) {
            $this->routes = cache(config('Cache.RoutesName'));
        } else {
            include_once root().'/App/routes.php';

            if (config('Cache.Routes')) {
                cache()->set(config('Cache.RoutesName'), $this->routes);
            }
        }

        $routes = [];
        foreach ($this->routes as $route) {
            $routes[] = [$route->method, $route->url, $route->action];
        }

        return $routes;
    }

    /**
     * Getter for the routes array.
     *
     * @return array
     */
    public function getRoutes(): array
    {
        return $this->routes;
    }

    /**
     * Takes the end of an url, and finds the prefix.
     *
     * @param string $url
     *
     * @return string
     */
    public function getRoutePrefix(string $url): string
    {
        foreach ($this->routes as $route) {
            if (s($route->url)->endsWith($url)) {
                return $route->url;
            }
        }

        return '';
    }

    /**
     * Return the url associated with an action (ex. PageController@index).
     * If a replacement is given the url variable between Curly brackets will be replaced with the $replacement value.
     *
     * @param string       $action
     * @param string|array $replace
     *
     * @return string
     */
    public function getActionUrl(string $action, $replace = null): string
    {
        if (array_key_exists($action, $this->actions)) {
            $url = $this->routes[
                $this->actions[ $action ]
            ]->url;

            if ($replace === null) {
                return $url;
            }

            $replace_arr = is_array($replace) ? $replace : [$replace];
            $index = 0;

            return preg_replace_callback('/({.*?})/', function () use ($replace_arr, &$index) {
                $match = $replace_arr[$index];
                ++$index;

                return $match;
            }, $url);
        }

        return '';
    }

    /**
     * Return the action associated with an url.
     *
     * @param string $url
     * @param string $method
     *
     * @return string
     */
    public function getUrlAction(string $url, string $method): string
    {
        foreach ($this->routes as $route) {
            if ($route->url === $url && $route->method === $method) {
                if (is_callable($route->action)) {
                    return 'Callable';
                }

                return $route->action;
            }
        }

        return '';
    }

    /**
     * @param string $action
     *
     * @return string
     */
    public function getActionMethod(string $action): string
    {
        if (array_key_exists($action, $this->actions)) {
            return $this->routes
            [
                $this->actions[$action]
            ]->method;
        }

        return '';
    }

    /**
     * @return int
     */
    public function getRouteCount(): int
    {
        return $this->index + 1;
    }
}
