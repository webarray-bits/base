<?php
/**
 * Created by PhpStorm.
 * User: Lex Ruesink
 * Date: 3-2-2016
 * Time: 13:45.
 */

namespace Framework\Util;

use Framework\Config;

class HashManager
{
    /**
     * Create a hash from the given string.
     *
     * @param string $toHash
     *
     * @return string
     */
    public function make(string $toHash): string
    {
        return password_hash($toHash, PASSWORD_DEFAULT, config('App.HashOptions'));
    }

    /**
     * Check if a string matches a given hash.
     *
     * @param string $toMatch
     * @param string $hash
     *
     * @return bool
     */
    public function match(string $toMatch, string $hash): bool
    {
        return password_verify($toMatch, $hash);
    }

    /**
     * Returns whether the given string needs a rehash.
     *
     * @param string $hash
     *
     * @return bool
     */
    public function needsRehash(string $hash): bool
    {
        return password_needs_rehash($hash, PASSWORD_DEFAULT, config('App.HashOptions'));
    }

    /**
     * Will rehash the string if needed.
     *
     * @param string $toHash
     * @param string $hash
     *
     * @return string
     */
    public function rehashIfNeeded(string $toHash, string $hash): string
    {
        if ($this->needsRehash($hash)) {
            return $this->make($toHash);
        }

        return $hash;
    }
}
