<?php

namespace Framework\Util;

use ArrayAccess;

class DotArray
{
    /**
     * Determine whether the given value is array accessible.
     *
     * @param $value
     *
     * @return bool
     */
    public static function accessible($value) :bool
    {
        return is_array($value) || $value instanceof ArrayAccess;
    }

    /**
     * Flatten a multi-dimensional associative array with dots.
     *
     * @param array  $array
     * @param string $prepend
     *
     * @return array
     */
    public static function dot(array $array, $prepend = '') :array
    {
        $results = [];

        foreach ($array as $key => $value) {
            if (is_array($value) && !empty($value)) {
                $results = array_merge($results, self::dot($value, $prepend.$key.'.'));
            } else {
                $results[$prepend.$key] = $value;
            }
        }

        return $results;
    }

    public static function undot(array $array) :array
    {
        $results = [];

        foreach ($array as $key => $value) {
            self::array_set($results, $key, $value);
        }

        return $results;
    }

    private function array_set(&$array, $key, $value)
    {
        if (is_null($key)) {
            return $array = $value;
        }

        $keys = explode('.', $key);

        while (count($keys) > 1) {
            $key = array_shift($keys);
            // If the key doesn't exist at this depth, we will just create an empty array
            // to hold the next value, allowing us to create the arrays to hold final
            // values at the correct depth. Then we'll keep digging into the array.
            if (!isset($array[$key]) || !is_array($array[$key])) {
                $array[$key] = array();
            }

            $array = &$array[$key];
        }

        $array[array_shift($keys)] = $value;

        return $array;
    }

    public static function exists(array $array, string $key) :bool
    {
        if (!self::accessible($array)) {
            return false;
        }

        if (self::noDotExists($array, $key)) {
            return true;
        }

        foreach (explode('.', $key) as $segment) {
            if (self::accessible($array) && self::noDotExists($array, $segment)) {
                $array = $array[$segment];
            } else {
                return false;
            }
        }

        return true;
    }

    /**
     * Determine if the given key exists in the provided array.
     *
     * @param $array
     * @param $key
     *
     * @return bool
     */
    private static function noDotExists(array $array, string $key) :bool
    {
        if ($array instanceof ArrayAccess) {
            return $array->offsetExists($key);
        }

        return array_key_exists($key, $array);
    }

    /**
     * Add an element to an array using "dot" notation if it doesn't exist.
     *
     * @param $array
     * @param $key
     * @param $value
     *
     * @return mixed
     */
    public static function add(array $array, string $key, $value)
    {
        if (is_null(self::get($array, $key))) {
            self::set($array, $key, $value);
        }

        return $array;
    }

    /**
     * Set an array item to a given value using "dot" notation.
     *
     * If no key is given to the method, the entire array will be replaced.
     *
     * @param $array
     * @param $key
     * @param $value
     *
     * @return mixed
     */
    public static function set(array &$array, $key, $value)
    {
        if (is_null($key)) {
            return $array = $value;
        }

        $keys = explode('.', $key);

        while (count($keys) > 1) {
            $key = array_shift($keys);
            // If the key doesn't exist at this depth, we will just create an empty array
            // to hold the next value, allowing us to create the arrays to hold final
            // values at the correct depth. Then we'll keep digging into the array.
            if (!isset($array[$key]) || !is_array($array[$key])) {
                $array[$key] = [];
            }

            $array = &$array[$key];
        }

        $array[array_shift($keys)] = $value;

        return $array;
    }

    /**
     * Remove one or many array items from a given array using "dot" notation.
     *
     * @param $array
     * @param $keys
     */
    public static function forget(array &$array, string $keys)
    {
        $original = &$array;
        $keys = (array) $keys;

        if (count($keys) === 0) {
            return;
        }

        foreach ($keys as $key) {
            // if the exact key exists in the top-level, remove it
            if (self::noDotExists($array, $key)) {
                unset($array[$key]);
                continue;
            }

            $parts = explode('.', $key);
            // clean up before each pass
            $array = &$original;

            while (count($parts) > 1) {
                $part = array_shift($parts);

                if (isset($array[$part]) && is_array($array[$part])) {
                    $array = &$array[$part];
                } else {
                    continue 2;
                }
            }

            unset($array[array_shift($parts)]);
        }
    }

    /**
     * Get an item from an array using "dot" notation.
     *
     * @param array       $array
     * @param string|null $key
     * @param null        $default
     *
     * @return array|mixed|null
     */
    public static function get(array $array, string $key = null, $default = null)
    {
        if (!self::accessible($array)) {
            return $default;
        }

        if (is_null($key)) {
            return $array;
        }

        if (self::noDotExists($array, $key)) {
            return $array[$key];
        }

        foreach (explode('.', $key) as $segment) {
            if (self::accessible($array) && self::noDotExists($array, $segment)) {
                $array = $array[$segment];
            } else {
                return $default;
            }
        }

        return $array;
    }

    /**
     * Get a value from the array, and remove it.
     *
     * @param $array
     * @param $key
     * @param null $default
     *
     * @return array|mixed|null
     */
    public static function pull(array &$array, string $key, $default = null)
    {
        $value = self::get($array, $key, $default);
        self::forget($array, $key);

        return $value;
    }
}
