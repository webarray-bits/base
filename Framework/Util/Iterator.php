<?php

namespace Framework\Util;

class Iterator
{
    /**
     * Array on which the iterator works.
     *
     * @var array
     */
    protected $array;

    /**
     * Iterator constructor.
     *
     * @param $array
     */
    public function __construct($array)
    {
        if (!($array instanceof \ArrayAccess) && !is_array($array)) {
            throw new \InvalidArgumentException('Iterator expects an array or an object implementing \\ArrayAccess');
        }

        $this->array = $array;
    }

    /**
     * Calls the given function with each element in the iterator,
     * and storing the returned value as the new value for the element.
     *
     * @param callable $closure
     *
     * @return Iterator
     */
    public function map(callable $closure): Iterator
    {
        foreach ($this->array as $key => $value) {
            $this->array[$key] = $closure($value);
        }

        return $this;
    }

    /**
     * Removes all the elements in the iterator that don't pass the given filter.
     *
     * @param callable $closure
     *
     * @return Iterator
     */
    public function filter(callable $closure): Iterator
    {
        foreach ($this->array as $key => $value) {
            if ($closure($value) === false) {
                unset($this->array[$key]);
            }
        }

        return $this;
    }

    /**
     * Iterate over each element in the iterator,
     * passing the given function the current element and a reference to the accumulator
     * for each element in the iterator.
     *
     * The function definition for 'fold' should be as follows:
     *
     * function (&$acc, $value) {...}
     *
     * This function will return the accumulator.
     *
     * @param $acc
     * @param callable $closure
     *
     * @return mixed
     */
    public function fold($acc, callable $closure)
    {
        foreach ($this->array as $value) {
            // The acc should be passed by reference!
            $closure($acc, $value);
        }

        return $acc;
    }

    /**
     * Sum all the elements in the iterator.
     * It is required that all elements in the iterator are a number
     * for this function to work.
     *
     * @return Iterator
     */
    public function sum()
    {
        return $this->fold(0, function (&$acc, $value) {
            $acc += $value;
        });
    }

    /**
     * Check if at least one of the elements in the iterator match the given filter.
     *
     * @param callable $closure
     *
     * @return bool
     */
    public function any(callable $closure): bool
    {
        foreach ($this->array as $value) {
            if ($closure($value) === true) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if all the elements in the iterator match the given filter.
     *
     * @param callable $closure
     *
     * @return bool
     */
    public function all(callable $closure): bool
    {
        foreach ($this->array as $value) {
            if ($closure($value) === false) {
                return false;
            }
        }

        return true;
    }

    /**
     * Returns the amount of elements in the iterator.
     *
     * @return int
     */
    public function size(): int
    {
        return sizeof($this->array);
    }

    /**
     * Returns whether the iterator is empty.
     *
     * @return bool
     */
    public function empty(): bool
    {
        return sizeof($this->array) === 0;
    }

    /**
     * Return the nth element in the iterator.
     *
     * @param $key
     *
     * @return bool|mixed
     */
    public function nth($key)
    {
        if (!array_key_exists($key, $this->array)) {
            return false;
        }

        return $this->array[$key];
    }

    /**
     * Divide the current iterator into two groups, depending on the given filter.
     *
     * @param callable $closure
     *
     * @return array
     */
    public function partition(callable $closure): array
    {
        $first = [];
        $second = [];

        foreach ($this->array as $key => $value) {
            if ($closure($value) === true) {
                $first[$key] = $value;
            } else {
                $second[$key] = $value;
            }
        }

        return [array_values($first), array_values($second)];
    }

    /**
     * Append the given iterator to the current one.
     *
     * @param Iterator $i
     *
     * @return Iterator
     */
    public function chain(Iterator $i): Iterator
    {
        $this->array = array_merge($this->array, $i->end());

        return $this;
    }

    /**
     * Clone the current iterator.
     *
     * @return Iterator
     */
    public function clone(): Iterator
    {
        return it($this->array);
    }

    /**
     * End the iteration and return the final array or object.
     *
     * @return array|\ArrayAccess
     */
    public function end()
    {
        return is_array($this->array)
            ? array_values($this->array)
            : $this->array;
    }
}
