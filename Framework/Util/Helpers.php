<?php

/*
 * All helper variables that do not fit in a specific class go here.
 */

/*
 * All 'random' helper methods that do not fit in a specific class go here.
 */

/*
 * Time helper functions for readable time conversions.
 */

/**
 * From minutes to seconds.
 *
 * @param $minutes
 *
 * @return int
 */
function minutes($minutes)
{
    return (int) $minutes * 60;
}

/**
 * From hours to seconds.
 *
 * @param $hours
 *
 * @return int
 */
function hours($hours)
{
    return (int) $hours * 3600;
}

/**
 * From days to seconds.
 *
 * @param $days
 *
 * @return int
 */
function days($days)
{
    return (int) $days * 86400;
}

/**
 * From years to seconds.
 *
 * @param $years
 *
 * @return int
 */
function years($years)
{
    return (int) $years * 31536000;
}

/**
 * Die and dump given data.
 *
 * @param array ...$values
 */
function dd(...$values)
{
    dump($values);
    die();
}

/**
 * Take a relative uri and return the full url.
 *
 * @param string $uri
 *
 * @return string
 */
function url(string $uri)
{
    return $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].$uri;
}

/**
 * Define a get route. Used in App/routes.php.
 *
 * @param string $url
 * @param $action
 * @param array $middleware
 */
function get(string $url, $action, array $middleware = [])
{
    \Framework\Http\Endpoint::get($url, $action, $middleware);
}

/**
 * Define a post route. Used in App/routes.php.
 *
 * @param string $url
 * @param $action
 * @param array $middleware
 */
function post(string $url, $action, array $middleware = [])
{
    \Framework\Http\Endpoint::post($url, $action, $middleware);
}

/**
 * Define a delete route. Used in App/routes.php.
 *
 * @param string $url
 * @param $action
 * @param array $middleware
 */
function delete(string $url, $action, array $middleware = [])
{
    \Framework\Http\Endpoint::delete($url, $action, $middleware);
}

/**
 * Define a put route. Used in App/routes.php.
 *
 * @param string $url
 * @param $action
 * @param array $middleware
 */
function put(string $url, $action, array $middleware = [])
{
    \Framework\Http\Endpoint::put($url, $action, $middleware);
}

/**
 * Define a patch route. Used in App/routes.php.
 *
 * @param string $url
 * @param $action
 * @param array $middleware
 */
function patch(string $url, $action, array $middleware = [])
{
    \Framework\Http\Endpoint::patch($url, $action, $middleware);
}

/**
 * Takes an array of options and a closure.
 * All the options defined will be applied to the routes and groups nested within the closure.
 * Used in App/routes.php.
 *
 * @param array $options
 * @param $closure
 */
function group(array $options, Closure $closure)
{
    \Framework\Http\Endpoint::group($options, $closure);
}

/**
 * Defines all needed routes for a CRUD resource.
 * The options array can accept two options:
 * only: define only the given routes.
 * except: define all routes except the given routes.
 *
 * @param string $name
 * @param string $controller
 * @param array  $middleware
 */
function resource(string $name, string $controller, array $middleware = [])
{
    $defaults = ['index', 'create', 'store', 'show', 'edit', 'update', 'destroy'];
    $final = array_intersect($defaults, get_class_methods("\\App\\Controllers\\$controller"));

    foreach ($final as $method) {
        switch ($method) {
            case 'index':
                get("/$name", "$controller@index", $middleware);
                break;
            case 'create':
                get("/$name/create", "$controller@create", $middleware);
                break;
            case 'store':
                post("/$name", "$controller@store", $middleware);
                break;
            case 'show':
                get("/$name/{key}", "$controller@show", $middleware);
                break;
            case 'edit':
                get("/$name/{key}/edit", "$controller@edit", $middleware);
                break;
            case 'update':
                put("/$name/{key}", "$controller@update", $middleware);
                break;
            case 'destroy':
                delete("/$name/{key}", "$controller@destroy", $middleware);
                break;
        }
    }
}

/**
 * Generates a variable dump from the given variables and returns the captured result.
 *
 * @param array ...$values
 *
 * @return string
 */
function dump_to_string(...$values): string
{
    ob_start();
    var_dump($values);

    return ob_get_clean();
}


/*
 * Filters and functional helpers.
 */

function even()
{
    return function ($v) {
        return $v % 2 === 0;
    };
}

function odd()
{
    return function ($v) {
        return $v % 2 === 1;
    };
}

function divisible_by($n)
{
    return function ($v) use ($n) {
        return $v % $n === 0;
    };
}

function it($array)
{
    return new Framework\Util\Iterator($array);
}

function map($array, callable $callable): array
{
    $results = [];

    if (!is_array($array)) {
        $results[] = $callable($array);
    } else {
        foreach ($array as $item) {
            $results[] = $callable($item);
        }
    }

    return $results;
}

/**
 * Retrieve the applications root directory.
 *
 * @return string
 */
function root(): string
{
    // App.Path is set dynamically in the kernel.
    return config('App.Path');
}