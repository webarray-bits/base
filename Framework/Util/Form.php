<?php

namespace Framework\Util;

use Framework\Container;
use Framework\Http\RouteRegistry;
use function Stringy\create as s;

class Form
{
    /**
     * A helper to create a form.
     * You can pass the following options to this:.
     *
     * method: Should always be passed, except when the 'route' option is used (as 'route' determines the method for you).
     *         Method can be POST, GET, PUT and DELETE. For PUT and DELETE a hidden input will be automatically added.
     *
     * url: Mutually exclusive with 'route' and 'action'. This is simply the url as you would set normally in the form action.
     *
     * action: Mutually exclusive with 'route' and 'url'. The action refers to the controller action which is associated with an endpoint.
     *         Example: in your routes.php the route 'get("/user/create", "UserController@create") exists. If your specify
     *         'action' => 'UserController@create', the matching url will be automatically placed in the form action.
     *         This will also resolve the method for you.
     *
     * route: Mutually exclusive with 'route' and 'url'. Route is useful when references a resource. A dotted resource action
     *        can be given, and the url and method will be automatically resolved.
     *        Example: You have a resource called 'user' in your routes.php. You have a form that wants to update (save) the user.
     *        By specifying 'route' => ['user.update', $id] the url will be automatically placed in the form action and the method will be resolved.
     *        'route' cannot be combined with 'method'.
     *        If no data is required (no wildcard in the route), route can simply be a string.
     *        Example: 'route' => 'user.index' will work because 'index' does not require additional data.
     *
     * files: Setting this to a value (it checks for existence) the enctype of the form will be set to 'multipart/form-data'.
     *
     * class: Sets the form class.
     *
     * id: Sets the form id.
     *
     * @param array $options
     *
     * @return string
     */
    public function create($options = ['method' => 'GET', 'url' => '#']): string
    {
        $url = '';
        $method = '';
        $hidden = '';
        $enctype = '';
        $class = array_key_exists('class', $options) ? 'class="'.$options['class'].'"' : '';
        $style = array_key_exists('style', $options) ? 'style="'.$options['style'].'"' : '';
        $id = array_key_exists('id', $options) ? 'id="'.$options['id'].'"' : '';
        $html = array_key_exists('html', $options) ? $options['html'] : '';

        if (array_key_exists('url', $options)) {
            $url = $options['url'];
        } elseif (array_key_exists('action', $options)) {
            $action = is_string($options['action'])
                ? $options['action']     // Action without replace.
                : $options['action'][0]; // Action with $eplace.

            $replace = is_array($options['action'])
                ? $options['action'][1]  // Action with replace.
                : null;                  // Action without replace.

            $url = Container::get(RouteRegistry::class)->getActionUrl($action, $replace);
            $method = Container::get(RouteRegistry::class)->getActionMethod($action);
        } elseif (array_key_exists('route', $options)) {
            /*
             * Get route and data information.
             * Route can be a string, but also an array.
             */
            $route = strtolower(is_string($options['route'])
                    ? $options['route']     // Route without data.
                    : $options['route'][0]); // Route with data.

            $data = is_array($options['route'])
                    ? $options['route'][1]  // Route with data.
                    : '';                   // Route without data.

            list($name, $endpoint) = explode('.', $route);

            /*
             * Set the correct url with the endpoint and data.
             */
            switch ($endpoint) {
                case 'index':   $url = "/$name"; break;
                case 'create':  $url = "/$name/create"; break;
                case 'store':   $url = "/$name"; break;
                case 'show':    $url = "/$name/{key}"; break;
                case 'edit':    $url = "/$name/{key}/edit"; break;
                case 'update':  $url = "/$name/{key}"; break;
                case 'destroy': $url = "/$name/{key}"; break;
            }

            // Get the prefix for this url.
            $url = s(Container::get(RouteRegistry::class)->getRoutePrefix($url))->replace('{key}', $data);

            /*
             * Set the correct method for the route.
             */
            if (in_array($endpoint, ['index', 'create', 'show', 'edit'])) {
                $method = 'GET';
            } elseif ($endpoint === 'store') {
                $method = 'POST';
            } elseif ($endpoint === 'update') {
                $method = 'PUT';
            } else { // Only the delete endpoint is left.
                $method = 'DELETE';
            }
        }

        /*
         * Value of 'files' does not matter.
         * If it is specified, add the enctype.
         * It is left default otherwise.
         */
        if (array_key_exists('files', $options)) {
            $enctype = 'enctype="multipart/form-data"';
        }

        /*
         * If 'route' was used, method may already be set.
         * Fill it otherwise.
         */
        if ($method === '') {
            $method = $options['method'];
        }

        /*
         * Take care of methods that HTML does not natively support.
         */
        if ($method !== 'GET' || $method !== 'POST') {
            $hidden = "<input type=\"hidden\" name=\"_METHOD\" value=\"$method\" />";
            $method = 'POST';
        }

        /*
         * Build the form in a single string and return.
         */
        return "<form $class $id $style $html action=\"$url\" method=\"$method\" $enctype> $hidden";
    }

    /**
     * End the form.
     * This also adds the csrf token to the form.
     *
     * @return string
     */
    public function end(): string
    {
        $token = csrf_token();

        return "<input type=\"hidden\" name=\"csrf_token\" value=\"$token\"/></form>";
    }
}
