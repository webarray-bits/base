<?php

use Framework\Container;

/**
 * Helper method to generate a response object.
 *
 * @return \Framework\Http\Response|Response
 */
function response(): \Framework\Http\Response
{
    return Container::get(\Framework\Http\Response::class);
}

/**
 * Helper method to generate a request object.
 *
 * @return \Framework\Http\Request
 */
function request(): \Framework\Http\Request
{
    return Container::get(\Framework\Http\Request::class);
}

/**
 * Create a middleware instance with the given arguments.
 *
 * @param string $class
 * @param array ...$arguments
 *
 * @return \Framework\Middleware\Proxy
 */
function middleware(string $class, ...$arguments)
{
    return new \Framework\Middleware\Proxy($class, $arguments);
}

/**
 * If a key is given, return the value. If the key is not found, returns $default.
 * When a key is not given, the InputManager instance is returned.
 *
 * @param string|null $key
 * @param null $default
 * @return mixed|\Framework\Data\InputManager
 */
function input(string $key = null, $default = null)
{
    if ($key === null) {
        return Container::get(InputManager::class);
    }

    return Container::get(Framework\Data\InputManager::class)->get($key, $default);
}

/**
 * Retrieve an item from the configuration.
 *
 * @param string $key
 * @param null   $default
 *
 * @return mixed
 */
function config(string $key, $default = null)
{
    $value = app()->config($key);

    if ($value === null) {
        return $default;
    }

    return $value;
}

/**
 * Returns the File logger for the application.
 *
 * @return \Monolog\Logger
 */
function logger(): \Monolog\Logger
{
    return Container::get(\Monolog\Logger::class);
}

/**
 * Shortcut to resolve a class out of the service container.
 * If no arguments are given, the current application instance is returned.
 *
 * @param string|null $key
 *
 * @return mixed|\Framework\Application
 */
function app($key = null)
{
    if ($key === null) {
        return Container::get('app');
    }

    return Container::get($key);
}

/**
 * Retrieves an environment variable and
 * parses it to the correct type.
 *
 * @param string $key
 * @param null   $default
 *
 * @return mixed
 *
 * @throws Exception
 */
function env(string $key, $default = null)
{
    $value = getenv($key);

    if ($value === false) {
        if ($default === null) {
            throw new \Exception("Environment variable $key not set in .env.");
        } else {
            return $default;
        }
    }

    if (is_numeric($value)) {
        return $value + 0;
    } // Quick string to number conversion trick

    if (in_array($value, ['yes', 'no', 'true', 'false'])) {
        return filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    return $value;
}