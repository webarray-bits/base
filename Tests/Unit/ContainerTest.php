<?php

use Framework\Container;
use Framework\Service\ServiceRegistrar;

class ContainerTest extends PHPUnit_Framework_TestCase
{
    public function testCanResolve()
    {
        $instance = Container::get(ServiceRegistrar::class);
        $this->assertTrue($instance instanceof ServiceRegistrar);
    }
}
