<?php

include __DIR__.'/../vendor/autoload.php';

(new Framework\Kernel())->boot(__dir__.'/', true);
